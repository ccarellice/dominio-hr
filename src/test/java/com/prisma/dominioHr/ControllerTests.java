package com.prisma.dominioHr;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.sql.SQLIntegrityConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.prisma.dominioHr.controller.DeviceController;
import com.prisma.dominioHr.controller.UserController;
import com.prisma.dominioHr.dto.DeviceDTO;
import com.prisma.dominioHr.dto.UserDTO;
import com.prisma.dominioHr.entity.User;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ControllerTests {
	
	@Autowired
    private MockMvc mvc;
	
	private ObjectMapper objectMapper = new ObjectMapper();

	@Test
	public void a_insertUser() {
		
		UserDTO userDto = new UserDTO();
		userDto.setName("Flavio");
		userDto.setSurname("Ceccarelli");
		userDto.setCF("CCCLFV00P22H501V");
		userDto.setEmail("flavioceccarelli2019@gmail.com");
		userDto.setPassword("sas");
		userDto.setUsername("@ccarellice");
		userDto.setStatus(true);
		userDto.setRole("ub");
		
//		UserDTO userDto = UserDTO.builder()
//				.name("Flavio")
//				.surname("Ceccarelli")
//				.CF("CCCLFV00P22H501V")
//				.email("flavioceccarelli2019@gmail.com")
//				.password("sas")
//				.username("@ccarellice")
//				.status(true)
//				.role("ub")
//				.build();
		
		try {
			mvc.perform(post("/user-controller/insert-user")
					 .contentType(MediaType.APPLICATION_JSON)
					 .content(objectMapper.writeValueAsString(userDto)))
			 		 .andExpect(status().isCreated());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void b_insertUserError() {
		
		UserDTO userDto = new UserDTO();
		userDto.setName("Flavio");
		userDto.setSurname("Ceccarelli");
		userDto.setCF("CCCLFV00P22H501V");
		userDto.setEmail("flavioceccarelli2019@gmail.com");
		userDto.setPassword("sas");
		userDto.setUsername("@ccarellice");
		userDto.setStatus(true);
		userDto.setRole("ub");
		
//		User user = User.builder()
//				.name("Flavio")
//				.surname("Ceccarelli")
//				.CF("CCCLFV00P22H501V")
//				.email("flavioceccarelli2019@gmail.com")
//				.password("sas")
//				.username("@ccarellice")
//				.status(true)
//				.role(roleRepository.findByDescription("ub"))
//				.build();
		
		try {
			mvc.perform(post("/user-controller/insert-user")
					 .contentType(MediaType.APPLICATION_JSON)
					 .content(objectMapper.writeValueAsString(userDto)))
			 		 .andExpect(status().isCreated());
		} catch (Exception e) {
			Exception e1 = (Exception) e.getCause().getCause();
			if (e1 instanceof SQLIntegrityConstraintViolationException) {
				assertEquals(1062, ((SQLIntegrityConstraintViolationException) e1).getErrorCode());
			}
		}
	}
	
	@Test
	 public void c_findUserByCF() {
		 try {
			 mvc.perform(get("/user-controller/find-user-by-cf")
					 	.param("CF", "CCCLFV00P22H501V"))
		 				.andExpect(status().isOk())
		 				.andExpect(jsonPath("$.name", is("Flavio")))
		 				.andExpect(jsonPath("$.surname", is("Ceccarelli")));
		 } catch (Exception e) {
			 e.printStackTrace();
		 }
		 
	 }
	
	@Test
	public void d_deleteUser() {
		try {
			 mvc.perform(get("/user-controller/delete-user-by-cf")
					 	.param("CF", "CCCLFV00P22H501V"))
		 				.andExpect(status().isOk());
		 } catch (Exception e) {
			 e.printStackTrace();
		 }
	}
	
	
	
	@Test
	public void g_deleteFile() {
		try {
			 mvc.perform(get("/file-controller/delete-file-by-idfile")
					 	.param("idFile", "20"))
		 				.andExpect(status().isOk());
		 } catch (Exception e) {
			 e.printStackTrace();
		 }
	}
	
	@Test
	public void q_insertDevice() {
		
		DeviceDTO deviceDto = new DeviceDTO();
		deviceDto.setDescription("Laptop");
		deviceDto.setIdCode("A0086");
		deviceDto.setSerialNumber("ANTANICOMEFOSSE");
		deviceDto.setUserAssignedDevice("CCCFLV00P22H501V");
		deviceDto.setUserCespitiDevice("PLTPPP66B10H501A");
		
//		DeviceDTO deviceDto = DeviceDTO.builder()
//				.description("Laptop")
//				.idCode("A0086")
//				.serialNumber("ANTANICOMEFOSSE")
//				.userAssignedDevice("CCCFLV00P22H501V")
//				.userCespitiDevice("PLTPPP66B10H501A")
//				.build();

		try {
			mvc.perform(post("/device-controller/assign-device")
					 .contentType(MediaType.APPLICATION_JSON)
					 .content(objectMapper.writeValueAsString(deviceDto)))
			 		 .andExpect(status().isCreated());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void u_deleteDevice() {
		try {
			 mvc.perform(get("/device-controller/delete-device-by-idcode")
					 	.param("idCode", "A0086"))
		 				.andExpect(status().isOk());
		 } catch (Exception e) {
			 e.printStackTrace();
		 }
	}
}
