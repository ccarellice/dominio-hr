package com.prisma.dominioHr;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.sql.SQLIntegrityConstraintViolationException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import com.prisma.dominioHr.component.DeviceComponent;
import com.prisma.dominioHr.component.FileComponent;
import com.prisma.dominioHr.component.LabelComponent;
import com.prisma.dominioHr.component.TypeComponent;
import com.prisma.dominioHr.component.UserComponent;
import com.prisma.dominioHr.converter.DeviceConverter;
import com.prisma.dominioHr.converter.FileConverter;
import com.prisma.dominioHr.converter.UserConverter;
import com.prisma.dominioHr.dto.DeviceDTO;
import com.prisma.dominioHr.dto.FileDTO;
import com.prisma.dominioHr.dto.UserDTO;
import com.prisma.dominioHr.entity.Device;
import com.prisma.dominioHr.entity.File;
import com.prisma.dominioHr.entity.Label;
import com.prisma.dominioHr.entity.Type;
import com.prisma.dominioHr.entity.User;
import com.prisma.dominioHr.utils.DateConverter;

import lombok.Builder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class ComponentTests {
	
	@Autowired
	public static final Logger LOGGER = LoggerFactory.getLogger(ComponentTests.class);
	
	@Autowired
	private UserComponent userComponent;
	
	@Autowired
	private UserConverter userConverter;
	
	@Autowired
	private FileConverter fileConverter;
	
	@Autowired
	private FileComponent fileComponent;
	
	@Autowired
	private TypeComponent typeComponent;
	
	@Autowired
	private LabelComponent labelComponent;
	
	@Autowired
	private DeviceComponent deviceComponent;

	@Autowired
	DeviceConverter deviceConverter;

	
	@Test
	public void a_insertUser() {
		
		UserDTO userDto = new UserDTO();
		userDto.setName("Flavio");
		userDto.setSurname("Ceccarelli");
		userDto.setCF("CCCLFV00P22H501V");
		userDto.setEmail("flavioceccarelli2019@gmail.com");
		userDto.setPassword("sas");
		userDto.setUsername("@ccarellice");
		userDto.setStatus(true);
		userDto.setRole("ub");
		
//		UserDTO userDto = UserDTO.builder()
//				.name("Flavio")
//				.surname("Ceccarelli")
//				.CF("CCCLFV00P22H501V")
//				.email("flavioceccarelli2019@gmail.com")
//				.password("sas")
//				.username("@ccarellice")
//				.status(true)
//				.role("ub")
//				.build();
		
		Long idUser = userComponent.saveUser(userDto);
		assertNotNull(idUser);
	}
	
	@Test
	public void b_insertUserError() {
		
		UserDTO userDto = new UserDTO();
		userDto.setName("Flavio");
		userDto.setSurname("Ceccarelli");
		userDto.setCF("CCCLFV00P22H501V");
		userDto.setEmail("flavioceccarelli2019@gmail.com");
		userDto.setPassword("sas");
		userDto.setUsername("@ccarellice");
		userDto.setStatus(true);
		userDto.setRole("ub");
		
//		UserDTO userDto = UserDTO.builder()
//				.name("Flavio")
//				.surname("Ceccarelli")
//				.CF("CCCLFV00P22H501V")
//				.email("flavioceccarelli2019@gmail.com")
//				.password("sas")
//				.username("@ccarellice")
//				.status(true)
//				.role("ub")
//				.build();
		
		try {
			userComponent.saveUser(userDto);
		} catch (Exception e) {
			if(e instanceof DataIntegrityViolationException && e.getCause() != null && ((Exception) e.getCause()) instanceof ConstraintViolationException && e.getCause().getCause() != null 
									&& ((Exception) e.getCause().getCause()) instanceof SQLIntegrityConstraintViolationException) {
				assertEquals(1062, ((SQLIntegrityConstraintViolationException) e.getCause().getCause()).getErrorCode());
			}else {
				e.printStackTrace();
			}
		}
	}
	
	@Test
	public void c_findUserByCF() {
		User user = userComponent.findByCF("CCCFLV00P22H501V");
		assertEquals(user.getName(), "Flavio");
		assertEquals(user.getSurname(), "Ceccarelli");
		assertEquals(user.getEmail(), "flavioceccarelli2019@gmail.com");
		assertEquals(user.getUsername(), "@ccarellice");
		assertEquals(user.getCF(), "CCCFLV00P22H501V");
		assertEquals(user.getRole().getDescription(), "ub");
	}

	@Test
	public void d_deleteUser() {
		userComponent.deleteUserByCF("CCCFLV00P22H501V");
		User user = userComponent.findByCF("CCCFLV00P22H501V");
		assertNull(user);
	}
	
	@Test
	public void f_insertFile() {
	
		FileDTO fileDto = new FileDTO();
		fileDto.setUploadDate(DateConverter.fromLocalDateTimeToString(LocalDateTime.now()));
		fileDto.setLastEditDate(DateConverter.fromLocalDateTimeToString(LocalDateTime.now()));
		fileDto.setUserUpload(userComponent.findByCF("CCCFLV00P22H501V").getCF());
		fileDto.setUserAssigned(userComponent.findByCF("CCCFLV00P22H501V").getCF());
		fileDto.setLabel("Busta Paga");
		fileDto.setType("PDF");
	
//		FileDTO fileDto = FileDTO.builder()
//				.uploadDate(DateConverter.fromLocalDateTimeToString(LocalDateTime.now()))
//				.lastEditDate(DateConverter.fromLocalDateTimeToString(LocalDateTime.now()))
//				.userUpload(userComponent.findByCF("CCCFLV00P22H501V").getCF())
//				.userAssigned(userComponent.findByCF("CCCFLV00P22H501V").getCF())
//				.label("Busta Paga")
//				.type("PDF")
//				.build();
		
		Long idFile = fileComponent.save(fileDto);
		assertNotNull(idFile);
	}
	
	@Test
	public void g_deleteFile() {
		fileComponent.deleteByIdFile(new Long(29));
		File file = fileComponent.findByIdFile(new Long(29));
		assertNull(file);
		
		
	}
	
	@Test
	public void h_findTypeByDescription() {
		Type type = typeComponent.findByDescription("PDF");
		assertNotNull(type);
		assertEquals("PDF", type.getDescription());
	}
	
	@Test
	public void i_findLabelByDescription() {
		Label label = labelComponent.findByDescription("Busta Paga");
		assertNotNull(label);
		assertEquals("Busta Paga", label.getDescription());
	}
	
	@Test
	public void l_countBustaPagaCurrentMonth() {
		LOGGER.info("Numero File Caricati: " + fileComponent.countBustaPagaCurrentMonth(LocalDate.now().getMonthValue(), LocalDate.now().getYear()));
	}
	
	@Test
	public void m_countUploadedFiles() {
		LOGGER.info("Numero File: " + fileComponent.count());
	}
	
	@Test
	public void n_findActiveUserWithNoBustaPagaOnCurrentMonth() {
		List<User> users = userComponent.findActiveStatusUserWithNoBustaPagaOnCurrentMonth(LocalDate.now().getMonthValue(), LocalDate.now().getYear());
		for (User user : users) {
			LOGGER.info(user.getCF());
		}
	}
	
	@Test
	public void o_countActiveUserWithNoBustaPagaOnCurrentMonth() {
		Integer countUser = userComponent.countActiveStatusUserWithNoBustaPagaOnCurrentMonth(LocalDate.now().getMonthValue(), LocalDate.now().getYear());
		LOGGER.info("Numero Utenti Attivi Senza Busta Paga In Questo Mese: " + countUser );
	}
	
	@Test
	public void p_findLast5FileByIdUser() {
		long idUser = 54;
		Page<File> files = fileComponent.findLast5FileByUserId(PageRequest.of(0, 5), idUser);
		int contatore = 0;
		for (File file : files) {
			contatore++;
			LOGGER.info("File " + contatore + ": " + file.getIdFile());
		}
	}
	
	@Test
	public void q_insertDevice() {
		
		DeviceDTO deviceDto = new DeviceDTO();
		deviceDto.setDescription("Laptop");
		deviceDto.setIdCode("A0013");
		deviceDto.setSerialNumber("TARAPIOTAPIOCO");
		deviceDto.setUserCespitiDevice(userComponent.findByCF("PLTPPP66B10H501A").getCF());
		deviceDto.setUserAssignedDevice(userComponent.findByCF("CNZFVN84H15D643W").getCF());
		
//		DeviceDTO deviceDto = DeviceDTO.builder()
//				.description("Laptop")
//				.idCode("A0013")
//				.serialNumber("TARAPIOTAPIOCO")
//				.userCespitiDevice(userComponent.findByCF("PLTPPP66B10H501A").getCF())
//				.userAssignedDevice(userComponent.findByCF("CNZFVN84H15D643W").getCF())
//				.build();
		
		Long idDevice = deviceComponent.saveDevice(deviceDto);
		assertNotNull(idDevice);
	}
	
	@Test
	public void r_insertDeviceError() {
		
		DeviceDTO deviceDto = new DeviceDTO();
		deviceDto.setDescription("Laptop");
		deviceDto.setIdCode("A0013");
		deviceDto.setSerialNumber("TARAPIOTAPIOCO");
		deviceDto.setUserCespitiDevice(userComponent.findByCF("PLTPPP66B10H501A").getCF());
		deviceDto.setUserAssignedDevice(userComponent.findByCF("CNZFVN84H15D643W").getCF());
		
//		DeviceDTO deviceDto = DeviceDTO.builder()
//				.description("Laptop")
//				.idCode("A0013")
//				.serialNumber("TARAPIOTAPIOCO")
//				.userCespitiDevice(userComponent.findByCF("PLTPPP66B10H501A").getCF())
//				.userAssignedDevice(userComponent.findByCF("CNZFVN84H15D643W").getCF())
//				.build();

		try {
			deviceComponent.saveDevice(deviceDto);
		} catch (Exception e) {
			Exception e1 = (Exception) e.getCause().getCause();
			if (e1 instanceof SQLIntegrityConstraintViolationException) {
				assertEquals(1062, ((SQLIntegrityConstraintViolationException) e1).getErrorCode());
			}
		}
	}
	
	@Test
	@Transactional	
	public void s_findDeviceByIdCode() {
		DeviceDTO deviceDto = deviceComponent.findByIdCode("A0013");
		assertNotNull(deviceDto);
		assertEquals("A0013", deviceDto.getIdCode());
		assertEquals("Laptop", deviceDto.getDescription());
		assertEquals("TARAPIOTAPIOCO", deviceDto.getSerialNumber());
		assertEquals("PLTPPP66B10H501A", deviceDto.getUserCespitiDevice());
		assertEquals("CNZFVN84H15D643W", deviceDto.getUserAssignedDevice());
	}
	
	@Test
	@Transactional	
	public void t_findDeviceBySerialNumber() {
		Device device = deviceComponent.findBySerialNumber("TARAPIOTAPIOCO");
		assertNotNull(device);
		assertNotNull(device.getIdDevice());
		assertEquals("A0013", device.getIdCode());
		assertEquals("Laptop", device.getDescription());
		assertEquals("TARAPIOTAPIOCO", device.getSerialNumber());
		assertEquals("PLTPPP66B10H501A", device.getUserCespitiDevice().getCF());
		assertEquals("CNZFVN84H15D643W", device.getUserAssignedDevice().getCF());
	}
	
	@Test
	public void u_deleteDevice() {
		deviceComponent.deleteDevice(deviceComponent.findByIdCode("A0013"));
		DeviceDTO deviceDto = deviceComponent.findByIdCode("A0013");
		assertNull(deviceDto);
	}
	
	@Test
	public void v_findDeviceByUserCF() {
		List<Device> devices = deviceComponent.findDeviceByUserCFAssigned("CNZFVN84H15D643W");
		Integer contatore = 0;
		for (Device device : devices) {
			contatore++;
			LOGGER.info("Dispositivo " + contatore + ": " + device.getDescription() + ", " + device.getIdCode() + ", " + device.getSerialNumber());
		}
		
	}
	
	@Test
	public void x1_updateDevice() {
		DeviceDTO deviceDto = deviceComponent.findByIdCode("A0013");
		deviceDto.setSerialNumber("TISCANNOCOMPA");
		deviceComponent.updateDevice(deviceDto);
	}
	
	@Test
	public void x2_updateFile() {
		FileDTO fileDto = fileConverter.fromFileToDTO(fileComponent.findByIdFile(new Long(29)));
		fileDto.setLastEditDate(DateConverter.fromLocalDateTimeToString(LocalDateTime.now()));
		fileComponent.updateFile(fileDto);
	}
	
	@Test
	public void x3_updateUser() {
		UserDTO userDto = userConverter.fromUserToDTO(userComponent.findByCF("CCCFLV00P22H501V"));
		userDto.setStatus(false);
		userComponent.updateUser(userDto);
	}
	
	@Test
	public void x4_findLastXFiles() {
		int x = 5;
		Page<File> pagedFile = fileComponent.findPagedFile(PageRequest.of(0, x));
		for (File file : pagedFile) {
			LOGGER.info("Sas: " + file.getIdFile());
		}
	}
}
