package com.prisma.dominioHr;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.sql.SQLIntegrityConstraintViolationException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import com.prisma.dominioHr.converter.DeviceConverter;
import com.prisma.dominioHr.dto.DeviceDTO;
import com.prisma.dominioHr.entity.Device;
import com.prisma.dominioHr.entity.File;
import com.prisma.dominioHr.entity.Label;
import com.prisma.dominioHr.entity.Role;
import com.prisma.dominioHr.entity.Type;
import com.prisma.dominioHr.entity.User;
import com.prisma.dominioHr.repository.DeviceRepository;
import com.prisma.dominioHr.repository.FileRepository;
import com.prisma.dominioHr.repository.LabelRepository;
import com.prisma.dominioHr.repository.RoleRepository;
import com.prisma.dominioHr.repository.TypeRepository;
import com.prisma.dominioHr.repository.UserRepository;
import com.prisma.dominioHr.service.UserService;

import lombok.extern.slf4j.Slf4j;


@RunWith(SpringRunner.class)
@ComponentScan(basePackages = {"com.prisma"} )
@SpringBootTest
@Slf4j
public class RepositoryTests {

	@Autowired
	public static final Logger LOGGER = LoggerFactory.getLogger(RepositoryTests.class);
	
	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private FileRepository fileRepository;

	@Autowired
	private TypeRepository typeRepository;

	@Autowired
	private LabelRepository labelRepository;
	
	@Autowired
	private DeviceRepository deviceRepository;
	@Autowired
	private DeviceConverter deviceConverter;

	@Test
	public void a_insertUser() {
		//insert user Fioravante
//		User user = new User();
//		user.setName("Fioravante");
//		user.setSurname("Canzano");
//		user.setCF("CZNFRV87P22H501P");
//		user.setEmail("fioravante.canzano@gmail.com");
//		user.setPassword("sas");
//		user.setUsername("@flower");
//		user.setStatus(true);
//		user.setRole(roleRepository.findByDescription("ub"));
		
		//insert user Flavio
		User user = new User();
		user.setName("Flavio");
		user.setSurname("Ceccarelli");
		user.setCF("CCCLFV00P22H501V");
		user.setEmail("flavioceccarelli2019@gmail.com");
		user.setPassword("sas");
		user.setUsername("@ccarellice");
		user.setStatus(true);
		user.setRole(roleRepository.findByDescription("ub"));
		
//		User user = User.builder()
//				.name("Flavio")
//				.surname("Ceccarelli")
//				.CF("CCCLFV00P22H501V")
//				.email("flavioceccarelli2019@gmail.com")
//				.password("sas")
//				.username("@ccarellice")
//				.status(true)
//				.role(roleRepository.findByDescription("ub"))
//				.build();
				
		User inserted = userRepository.saveAndFlush(user);

		assertNotNull(inserted);
		assertNotNull(inserted.getIdUser());
	}

	@Test
	public void b_insertUserError() {
		
		User user = new User();
		user.setName("Flavio");
		user.setSurname("Ceccarelli");
		user.setCF("CCCLFV00P22H501V");
		user.setEmail("flavioceccarelli2019@gmail.com");
		user.setPassword("sas");
		user.setUsername("@ccarellice");
		user.setStatus(true);
		user.setRole(roleRepository.findByDescription("ub"));
		
//		User user = User.builder()
//				.name("Flavio")
//				.surname("Ceccarelli")
//				.CF("CCCLFV00P22H501V")
//				.email("flavioceccarelli2019@gmail.com")
//				.password("sas")
//				.username("@ccarellice")
//				.status(true)
//				.role(roleRepository.findByDescription("ub"))
//				.build();

		try {
			userRepository.saveAndFlush(user);
		} catch (Exception e) {
			Exception e1 = (Exception) e.getCause().getCause();
			if (e1 instanceof SQLIntegrityConstraintViolationException) {
				assertEquals(1062, ((SQLIntegrityConstraintViolationException) e1).getErrorCode());
			}
		}
	}

	@Test
	@Transactional
	public void c_findUserByCF() {
		User user = userRepository.findByCF("CCCFLV00P22H501P");
		assertNotNull(user);
		assertEquals("CCCFLV00P22H501P", user.getCF());
		assertNotNull(user.getRole());
		assertEquals("Flavio", user.getName());
		assertEquals("Ceccarelli", user.getSurname());
		assertEquals("@ccarellice", user.getUsername());
		assertEquals("flavioceccarelli2019@gmail.com", user.getEmail());
	}

	@Test
	public void d_deleteUser() {
		User user = userRepository.findByCF("CCCFLV00P22H501P");
		userRepository.delete(user);
		user = userRepository.findByCF("CCCFLV00P22H501P");
		assertNull(user);
	}

	@Test
	public void e_findRoleByDescription() {
		Role role = roleRepository.findByDescription("cespiti");
		assertNotNull(role);
		assertEquals("cespiti", role.getDescription()); 
	}

	@Test
	public void f_insertFile(){
		
		File file = new File();
		file.setUploadDate(LocalDateTime.now());
		file.setLastEditDate(LocalDateTime.now());
		file.setUserUpload(userRepository.findByCF("CCCFLV00P22H501V"));
		file.setUserAssigned(userRepository.findByCF("CCCFLV00P22H501V"));
		file.setLabel(labelRepository.findByDescription("Busta Paga"));
		file.setType(typeRepository.findByDescription("PDF"));
		
//		File file = File.builder()
//				.uploadDate(LocalDateTime.now())
//				.lastEditDate(LocalDateTime.now())
//				.userUpload(userRepository.findByCF("CCCFLV00P22H501V"))
//				.userAssigned(userRepository.findByCF("CCCFLV00P22H501V"))
//				.label(labelRepository.findByDescription("Busta Paga"))
//				.type(typeRepository.findByDescription("PDF"))
//				.build();

		File inserted = fileRepository.saveAndFlush(file);

		assertNotNull(inserted);
		assertNotNull(inserted.getIdFile());
	}

	@Test
	public void g_deleteFile() {
		File file = fileRepository.findByIdFile(new Long(5));
		fileRepository.delete(file);
		file = fileRepository.findByIdFile(new Long(5));
		assertNull(file);
	}
	
	@Test
	public void h_findTypeByDescription() {
		Type type = typeRepository.findByDescription("PDF");
		assertNotNull(type);
		assertEquals("PDF", type.getDescription());
	}
	
	@Test
	public void i_findLabelByDescription() {
		Label label = labelRepository.findByDescription("Busta Paga");
		assertNotNull(label);
		assertEquals("Busta Paga", label.getDescription());
	}
	
	@Test
	public void l_countBustaPagaCurrentMonth() {
		LOGGER.info("Numero File Caricati: " + fileRepository.countBustaPagaCurrentMonth(LocalDate.now().getMonthValue(), LocalDate.now().getYear()));
	}
	
	@Test
	public void m_countUploadedFiles() {
		LOGGER.info("Numero File: " + fileRepository.count());
	}
	
	@Test
	public void n_findActiveUserWithNoBustaPagaOnCurrentMonth() {
		List<User> users = userRepository.findActiveStatusUserWithNoBustaPagaOnCurrentMonth(LocalDate.now().getMonthValue(), LocalDate.now().getYear());
		for (User user : users) {
			LOGGER.info(user.getCF());
		}
	}
	
	@Test
	public void o_countActiveUserWithNoBustaPagaOnCurrentMonth() {
		Integer countUser = userRepository.countActiveStatusUserWithNoBustaPagaOnCurrentMonth(LocalDate.now().getMonthValue(), LocalDate.now().getYear());
		LOGGER.info("Numero Utenti Attivi Senza Busta Paga In Questo Mese: " + countUser );
	}
	
	@Test
	public void p_findLast5FileByIdUser() {
		long idUser = 54;
		Page<File> files = fileRepository.findLast5FileByUserId(PageRequest.of(0, 5), idUser);
		int contatore = 0;
		for (File file : files) {
			contatore++;
			LOGGER.info("File " + contatore + ": " + file.getIdFile());
		}
	}
	
	@Test
	public void q_insertDevice() {
		
		Device device = new Device();
		device.setDescription("Laptop");
		device.setIdCode("A0013");
		device.setSerialNumber("TARAPIOTAPIOCO");
		device.setUserCespitiDevice(userRepository.findByCF("CCCLFV00P22H501V"));
		device.setUserAssignedDevice(userRepository.findByCF("CCCLFV00P22H501V"));
		
//		Device device = Device.builder()
//				.description("Laptop")
//				.idCode("A0013")
//				.serialNumber("TARAPIOTAPIOCO")
//				.userCespitiDevice(userRepository.findByCF("PLTPPP66B10H501A"))
//				.userAssignedDevice(userRepository.findByCF("CNZFVN84H15D643W"))
//				.build();
		
		Device inserted = deviceRepository.saveAndFlush(device);

		assertNotNull(inserted);
		assertNotNull(inserted.getIdDevice());
	}
	
	@Test
	public void r_insertDeviceError() {
		
		Device device = new Device();
		device.setDescription("Laptop");
		device.setIdCode("A0013");
		device.setSerialNumber("TARAPIOTAPIOCO");
		device.setUserCespitiDevice(userRepository.findByCF("PLTPPP66B10H501A"));
		device.setUserAssignedDevice(userRepository.findByCF("CNZFVN84H15D643W"));
		
//		Device device = Device.builder()
//				.description("Laptop")
//				.idCode("A0013")
//				.serialNumber("TARAPIOTAPIOCO")
//				.userCespitiDevice(userRepository.findByCF("PLTPPP66B10H501A"))
//				.userAssignedDevice(userRepository.findByCF("CNZFVN84H15D643W"))
//				.build();

		try {
			deviceRepository.saveAndFlush(device);
		} catch (Exception e) {
			Exception e1 = (Exception) e.getCause().getCause();
			if (e1 instanceof SQLIntegrityConstraintViolationException) {
				assertEquals(1062, ((SQLIntegrityConstraintViolationException) e1).getErrorCode());
			}
		}
	}
	
	@Test
	@Transactional	
	public void s_findDeviceByIdCode() {
		Device device = deviceRepository.findByIdCode("A0013");
		assertNotNull(device);
		assertNotNull(device.getIdDevice());
		assertEquals("A0013", device.getIdCode());
		assertEquals("Laptop", device.getDescription());
		assertEquals("TARAPIOTAPIOCO", device.getSerialNumber());
		assertEquals("PLTPPP66B10H501A", device.getUserCespitiDevice().getCF());
		assertEquals("CNZFVN84H15D643W", device.getUserAssignedDevice().getCF());
		
	}
	
	@Test
	@Transactional	
	public void t_findDeviceBySerialNumber() {
		Device device = deviceRepository.findBySerialNumber("TARAPIOTAPIOCO");
		assertNotNull(device);
		assertNotNull(device.getIdDevice());
		assertEquals("A0013", device.getIdCode());
		assertEquals("Laptop", device.getDescription());
		assertEquals("TARAPIOTAPIOCO", device.getSerialNumber());
		assertEquals("PLTPPP66B10H501A", device.getUserCespitiDevice().getCF());
		assertEquals("CNZFVN84H15D643W", device.getUserAssignedDevice().getCF());
	}
	
	@Test
	public void u_deleteDevice() {
		Device device = deviceRepository.findByIdCode("A0013");
		deviceRepository.delete(device);
		device = deviceRepository.findByIdCode("A0013");
		assertNull(device);
	}
	
	@Test
	public void v_findDeviceByUserCF() {
		List<Device> devices = deviceRepository.findDeviceByUserCFAssigned("CNZFVN84H15D643W");
		Integer contatore = 0;
		for (Device device : devices) {
			contatore++;
			LOGGER.info("Dispositivo " + contatore + ": " + device.getDescription() + ", " + device.getIdCode() + ", " + device.getSerialNumber());
		}
	}
}
