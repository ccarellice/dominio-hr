package com.prisma.dominioHr;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.sql.SQLIntegrityConstraintViolationException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import com.prisma.dominioHr.entity.Device;
import com.prisma.dominioHr.entity.File;
import com.prisma.dominioHr.entity.Label;
import com.prisma.dominioHr.entity.Role;
import com.prisma.dominioHr.entity.Type;
import com.prisma.dominioHr.entity.User;
import com.prisma.dominioHr.repository.TypeRepository;
import com.prisma.dominioHr.service.DeviceService;
import com.prisma.dominioHr.service.FileService;
import com.prisma.dominioHr.service.LabelService;
import com.prisma.dominioHr.service.RoleService;
import com.prisma.dominioHr.service.TypeService;
import com.prisma.dominioHr.service.UserService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class ServiceTests {
	
	@Autowired
	public static final Logger LOGGER = LoggerFactory.getLogger(ServiceTests.class);
	
	@Autowired
	UserService userService;
	
	@Autowired
	RoleService roleService;
	
	@Autowired
	LabelService labelService;
	
	@Autowired
	TypeService typeService;
	
	@Autowired
	FileService fileService;
	
	@Autowired
	DeviceService deviceService;
	
	
	@Test
	public void a_insertUser() {
		
		User user = new User();
		user.setName("Flavio");
		user.setSurname("Ceccarelli");
		user.setCF("CCCLFV00P22H501V");
		user.setEmail("flavioceccarelli2019@gmail.com");
		user.setPassword("sas");
		user.setUsername("@ccarellice");
		user.setStatus(true);
		user.setRole(roleService.findByDescription("ub"));
		
//		User user = User.builder()
//				.name("Flavio")
//				.surname("Ceccarelli")
//				.CF("CCCLFV00P22H501V")
//				.email("flavioceccarelli2019@gmail.com")
//				.password("sas")
//				.username("@ccarellice")
//				.status(true)
//				.role(roleService.findByDescription("ub"))
//				.build();

		
		User inserted;
		try {
			inserted = userService.save(user);
			assertNotNull(inserted);
			assertNotNull(inserted.getIdUser());
		}
		catch (Exception e) {
			LOGGER.error("Errore " + e.getMessage());
		}
	}
	
	@Test
	public void b_insertUserError() {
		
		User user = new User();
		user.setName("Flavio");
		user.setSurname("Ceccarelli");
		user.setCF("CCCLFV00P22H501V");
		user.setEmail("flavioceccarelli2019@gmail.com");
		user.setPassword("sas");
		user.setUsername("@ccarellice");
		user.setStatus(true);
		user.setRole(roleService.findByDescription("ub"));
		
//		User user = User.builder()
//				.name("Flavio")
//				.surname("Ceccarelli")
//				.CF("CCCLFV00P22H501V")
//				.email("flavioceccarelli2019@gmail.com")
//				.password("sas")
//				.username("@ccarellice")
//				.status(true)
//				.role(roleService.findByDescription("ub"))
//				.build();
		
		try {
			userService.save(user);
		} catch (Exception e) {
			if(e instanceof DataIntegrityViolationException && e.getCause() != null && ((Exception) e.getCause()) instanceof ConstraintViolationException && e.getCause().getCause() != null 
									&& ((Exception) e.getCause().getCause()) instanceof SQLIntegrityConstraintViolationException) {
				assertEquals(1062, ((SQLIntegrityConstraintViolationException) e.getCause().getCause()).getErrorCode());
			}else {
				e.printStackTrace();
			}
		}
	}
	
	@Test
	@Transactional
	public void c_findUserByCF() {
		User user = userService.findByCF("CCCFLV00P22H501V");
		assertNotNull(user);
		assertEquals("CCCFLV00P22H501V", user.getCF());
		assertNotNull(user.getRole());
		assertEquals("Flavio", user.getName());
		assertEquals("Ceccarelli", user.getSurname());
		assertEquals("@ccarellice", user.getUsername());
		assertEquals("flavioceccarelli2019@gmail.com", user.getEmail());
	}
	
	@Test
	public void d_deleteUser() {
		User user = userService.findByCF("CCCFLV00P22H501V");
		userService.delete(user);
		user = userService.findByCF("CCCFLV00P22H501V");
		assertNull(user);
	}
	
	@Test
	public void e_findRoleByDescription() {
		Role role = roleService.findByDescription("ub");
		assertNotNull(role);
		assertEquals("ub", role.getDescription());
	}
	
	@Test
	public void f_insertFile() {
		
		File file = new File();
		file.setUploadDate(LocalDateTime.now());
		file.setLastEditDate(LocalDateTime.now());
		file.setUserUpload(userService.findByCF("CCCFLV00P22H501V"));
		file.setUserAssigned(userService.findByCF("CCCFLV00P22H501V"));
		file.setLabel(labelService.findByDescription("Busta Paga"));
		file.setType(typeService.findByDescription("PDF"));
		
//		File file = File.builder()
//				.uploadDate(LocalDateTime.now())
//				.lastEditDate(LocalDateTime.now())
//				.userUpload(userService.findByCF("CCCFLV00P22H501V"))
//				.userAssigned(userService.findByCF("CCCFLV00P22H501V"))
//				.label(labelService.findByDescription("Busta Paga"))
//				.type(typeService.findByDescription("PDF"))
//				.build();

		File inserted;
		
		try {
			inserted = fileService.save(file);
			assertNotNull(inserted);
			assertNotNull(inserted.getIdFile());
		}
		catch (Exception e) {
			LOGGER.error("Errore " + e.getMessage());
		}
	}
	
	@Test
	public void g_deleteFile() {
		File file = fileService.findFileByIdFile(new Long(11));
		fileService.delete(file);
		file = fileService.findFileByIdFile(new Long(11));
		assertNull(file);
	}
	
	@Test
	public void h_findTypeByDescription() {
		Type type = typeService.findByDescription("PDF");
		assertNotNull(type);
		assertEquals("PDF", type.getDescription());
	}
	
	@Test
	public void i_findLabelByDescription() {
		Label label = labelService.findByDescription("Busta Paga");
		assertNotNull(label);
		assertEquals("Busta Paga", label.getDescription());
	}
	
	@Test
	public void l_countBustaPagaCurrentMonth() {
		LOGGER.info("Numero File Caricati: " + fileService.countBustaPagaCurrentMonth(LocalDate.now().getMonthValue(), LocalDate.now().getYear()));
	}
	
	@Test
	public void m_countUploadedFiles() {
		LOGGER.info("Numero File: " + fileService.count());
	}
	
	@Test
	public void n_findActiveUserWithNoBustaPagaOnCurrentMonth() {
		List<User> users = userService.findActiveStatusUserWithNoBustaPagaOnCurrentMonth(LocalDate.now().getMonthValue(), LocalDate.now().getYear());
		for (User user : users) {
			LOGGER.info(user.getCF());
		}
	}
	
	@Test
	public void o_countActiveUserWithNoBustaPagaOnCurrentMonth() {
		Integer countUser = userService.countActiveStatusUserWithNoBustaPagaOnCurrentMonth(LocalDate.now().getMonthValue(), LocalDate.now().getYear());
		LOGGER.info("Numero Utenti Attivi Senza Busta Paga In Questo Mese: " + countUser );
	}
	
	@Test
	public void p_findLast5FileByIdUser() {
		long idUser = 54;
		Page<File> files = fileService.findLast5FileByUserId(PageRequest.of(0, 5), idUser);
		int contatore = 0;
		for (File file : files) {
			contatore++;
			LOGGER.info("File " + contatore + ": " + file.getIdFile());
		}
	}
	
	@Test
	public void q_insertDevice() {
		
		Device device = new Device();
		device.setDescription("Laptop");
		device.setIdCode("A0013");
		device.setSerialNumber("TARAPIOTAPIOCO");
		device.setUserCespitiDevice(userService.findByCF("PLTPPP66B10H501A"));
		device.setUserAssignedDevice(userService.findByCF("CNZFVN84H15D643W"));
		
//		Device device = Device.builder()
//				.description("Laptop")
//				.idCode("A0013")
//				.serialNumber("TARAPIOTAPIOCO")
//				.userCespitiDevice(userService.findByCF("PLTPPP66B10H501A"))
//				.userAssignedDevice(userService.findByCF("CNZFVN84H15D643W"))
//				.build();
		
		Device inserted = deviceService.saveAndFlush(device);

		assertNotNull(inserted);
		assertNotNull(inserted.getIdDevice());
	}
	
	@Test
	public void r_insertDeviceError() {
		
		Device device = new Device();
		device.setDescription("Laptop");
		device.setIdCode("A0013");
		device.setSerialNumber("TARAPIOTAPIOCO");
		device.setUserCespitiDevice(userService.findByCF("PLTPPP66B10H501A"));
		device.setUserAssignedDevice(userService.findByCF("CNZFVN84H15D643W"));
		
//		Device device = Device.builder()
//				.description("Laptop")
//				.idCode("A0013")
//				.serialNumber("TARAPIOTAPIOCO")
//				.userCespitiDevice(userService.findByCF("PLTPPP66B10H501A"))
//				.userAssignedDevice(userService.findByCF("CNZFVN84H15D643W"))
//				.build();

		try {
			deviceService.saveAndFlush(device);
		} catch (Exception e) {
			Exception e1 = (Exception) e.getCause().getCause();
			if (e1 instanceof SQLIntegrityConstraintViolationException) {
				assertEquals(1062, ((SQLIntegrityConstraintViolationException) e1).getErrorCode());
			}
		}
	}
	
	@Test
	@Transactional	
	public void s_findDeviceByIdCode() {
		Device device = deviceService.findByIdCode("A0013");
		assertNotNull(device);
		assertNotNull(device.getIdDevice());
		assertEquals("A0013", device.getIdCode());
		assertEquals("Laptop", device.getDescription());
		assertEquals("TARAPIOTAPIOCO", device.getSerialNumber());
		assertEquals("PLTPPP66B10H501A", device.getUserCespitiDevice().getCF());
		assertEquals("CNZFVN84H15D643W", device.getUserAssignedDevice().getCF());
		
	}
	
	@Test
	@Transactional	
	public void t_findDeviceBySerialNumber() {
		Device device = deviceService.findBySerialNumber("TARAPIOTAPIOCO");
		assertNotNull(device);
		assertNotNull(device.getIdDevice());
		assertEquals("A0013", device.getIdCode());
		assertEquals("Laptop", device.getDescription());
		assertEquals("TARAPIOTAPIOCO", device.getSerialNumber());
		assertEquals("PLTPPP66B10H501A", device.getUserCespitiDevice().getCF());
		assertEquals("CNZFVN84H15D643W", device.getUserAssignedDevice().getCF());
	}
	
	@Test
	public void u_deleteDevice() {
		Device device = deviceService.findByIdCode("A0013");
		deviceService.delete(device);
		device = deviceService.findByIdCode("A0013");
		assertNull(device);
	}
	
	@Test
	public void v_findDeviceByUserCF() {
		List<Device> devices = deviceService.findDeviceByUserCFAssigned("CNZFVN84H15D643W");
		Integer contatore = 0;
		for (Device device : devices) {
			contatore++;
			LOGGER.info("Dispositivo " + contatore + ": " + device.getDescription() + ", " + device.getIdCode() + ", " + device.getSerialNumber());
		}
		
	}
	
}
