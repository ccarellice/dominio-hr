package com.prisma.dominioHr.entity;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

import com.mysql.cj.jdbc.Blob;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class File {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long idFile;
	
	private LocalDateTime uploadDate;
	private LocalDateTime lastEditDate;
	private String name;
	
	@Lob
    private byte[] data;
	
	@ManyToOne //relazione molti ad 1
	@JoinColumn(name = "code_user_upload" , referencedColumnName = "idUser")
	private User userUpload;
	
	@ManyToOne //relazione molti ad 1
	@JoinColumn(name = "code_user_assigned" , referencedColumnName = "idUser")
	private User userAssigned;
	
	@ManyToOne //relazione molti ad 1
	@JoinColumn(name = "code_label" , referencedColumnName = "idLabel")
	private Label label;
	
	@ManyToOne //relazione molti ad 1
	@JoinColumn(name = "code_type" , referencedColumnName = "idType")
	private Type type;

	public long getIdFile() {
		return idFile;
	}

	public void setIdFile(long idFile) {
		this.idFile = idFile;
	}

	public LocalDateTime getUploadDate() {
		return uploadDate;
	}

	public void setUploadDate(LocalDateTime uploadDate) {
		this.uploadDate = uploadDate;
	}

	public LocalDateTime getLastEditDate() {
		return lastEditDate;
	}

	public void setLastEditDate(LocalDateTime lastEditDate) {
		this.lastEditDate = lastEditDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

	public User getUserUpload() {
		return userUpload;
	}

	public void setUserUpload(User userUpload) {
		this.userUpload = userUpload;
	}

	public User getUserAssigned() {
		return userAssigned;
	}

	public void setUserAssigned(User userAssigned) {
		this.userAssigned = userAssigned;
	}

	public Label getLabel() {
		return label;
	}

	public void setLabel(Label label) {
		this.label = label;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}
	
	
}
