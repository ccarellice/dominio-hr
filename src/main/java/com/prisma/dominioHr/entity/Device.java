package com.prisma.dominioHr.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Device {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long idDevice;
	
	@Column(unique = true)
	private String idCode;
	
	private String description;
	
	@Column(unique = true)
	private String serialNumber;
	
	@ManyToOne
	@JoinColumn(name = "code_user_cespiti" , referencedColumnName = "idUser")
	private User userCespitiDevice;
	
	@ManyToOne 
	@JoinColumn(name = "code_user_assgined" , referencedColumnName = "idUser")
	private User userAssignedDevice;

	public long getIdDevice() {
		return idDevice;
	}

	public void setIdDevice(long idDevice) {
		this.idDevice = idDevice;
	}

	public String getIdCode() {
		return idCode;
	}

	public void setIdCode(String idCode) {
		this.idCode = idCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public User getUserCespitiDevice() {
		return userCespitiDevice;
	}

	public void setUserCespitiDevice(User userCespitiDevice) {
		this.userCespitiDevice = userCespitiDevice;
	}

	public User getUserAssignedDevice() {
		return userAssignedDevice;
	}

	public void setUserAssignedDevice(User userAssignedDevice) {
		this.userAssignedDevice = userAssignedDevice;
	}
	
	
}
