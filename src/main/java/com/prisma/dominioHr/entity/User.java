package com.prisma.dominioHr.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long idUser;
	
	private String name;
	private String surname;
	private String email;
	private String username;
	private String password;
	private boolean status; //false = cessato | true = attivo
	

	@Column(unique = true)
	private String CF;
	
	@ManyToOne //relazione molti ad 1
	@JoinColumn(name = "code_role" , referencedColumnName = "idRole") //name = nomeCodiceDB  referencedColumnName = campoReferenziato
	private Role role;
	
	@OneToMany(mappedBy = "userUpload", orphanRemoval = true) //mappedBy = nomeAttributoNelManyToOne  
	private List<File> fileUpload;
	
	@OneToMany(mappedBy = "userAssigned", orphanRemoval = true)
	private List<File> fileAssigned;
	
	//device{
	@OneToMany(mappedBy = "userCespitiDevice", orphanRemoval = true)
	private List<Device> deviceCespiti;
	
	@OneToMany(mappedBy = "userAssignedDevice", orphanRemoval = true)
	private List<Device> deviceAssigned;
	//}

	public long getIdUser() {
		return idUser;
	}

	public void setIdUser(long idUser) {
		this.idUser = idUser;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getCF() {
		return CF;
	}

	public void setCF(String cF) {
		CF = cF;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public List<File> getFileUpload() {
		return fileUpload;
	}

	public void setFileUpload(List<File> fileUpload) {
		this.fileUpload = fileUpload;
	}

	public List<File> getFileAssigned() {
		return fileAssigned;
	}

	public void setFileAssigned(List<File> fileAssigned) {
		this.fileAssigned = fileAssigned;
	}

	public List<Device> getDeviceCespiti() {
		return deviceCespiti;
	}

	public void setDeviceCespiti(List<Device> deviceCespiti) {
		this.deviceCespiti = deviceCespiti;
	}

	public List<Device> getDeviceAssigned() {
		return deviceAssigned;
	}

	public void setDeviceAssigned(List<Device> deviceAssigned) {
		this.deviceAssigned = deviceAssigned;
	}
	
	
}
