package com.prisma.dominioHr.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.prisma.dominioHr.component.DeviceComponent;
import com.prisma.dominioHr.dto.DeviceDTO;
import com.prisma.dominioHr.entity.Device;

import net.sf.jasperreports.engine.JRException;

@RestController
@RequestMapping("device-controller")
public class DeviceController {
	
	@Autowired
	private DeviceComponent deviceComponent;	
	
	@PostMapping("assign-device")
	public ResponseEntity<?> assignDevice(@RequestBody DeviceDTO deviceDto) {
		deviceComponent.saveDevice(deviceDto);
		return new ResponseEntity<String>(HttpStatus.CREATED);
	}
	
	@GetMapping("delete-device-by-idcode")
	public ResponseEntity<?> deleteDeviceByIdCode(@RequestParam String idCode){
		deviceComponent.deleteDevice(deviceComponent.findByIdCode(idCode));
		return new ResponseEntity<String>(HttpStatus.OK);
	}
	
	@GetMapping(value = "report/{format}")
	public String generateReport(@PathVariable String format) throws JRException, IOException {
		return deviceComponent.exportDevices(format);
	}

}
