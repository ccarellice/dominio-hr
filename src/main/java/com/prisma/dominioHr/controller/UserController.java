package com.prisma.dominioHr.controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.prisma.dominioHr.component.UserComponent;
import com.prisma.dominioHr.dto.UserDTO;
import com.prisma.dominioHr.entity.User;

import net.sf.jasperreports.engine.JRException;

@RestController
@RequestMapping("user-controller")
public class UserController {

	@Autowired
	private UserComponent userComponent;
	
	@PostMapping(value = "insert-user")
	public ResponseEntity<?> insertUser(@RequestBody UserDTO userDto){
		userComponent.saveUser(userDto); 
		return new ResponseEntity<String>(HttpStatus.CREATED);
	}
	
	@GetMapping(value = "find-user-by-cf")
	public ResponseEntity<?> insertUser(@RequestParam String CF){
		return new ResponseEntity<User>(userComponent.findByCF(CF), HttpStatus.OK);
	}
	
	@GetMapping(value = "delete-user-by-cf")
	public ResponseEntity<?> deleteUser(@RequestParam String CF){
		userComponent.deleteUserByCF(CF);
		return new ResponseEntity<String>(HttpStatus.OK);
	}
	
	@GetMapping(value = "get-users")
	public List<User> findAll(){
		return userComponent.findAll();
	}
	
	@GetMapping(value = "report/{format}")
	public String generateReport(@PathVariable String format) throws JRException, IOException {
		return userComponent.exportUsers(format);
	}

}
