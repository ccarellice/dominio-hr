package com.prisma.dominioHr.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("dominiohr-controller")
public class DominioHRController {
	
//	@Autowired
//	@Qualifier(value = "operazioni1")
//	private OperazioniComponent operazioniComponent;
//	
//	@Autowired
//	private PersonaComponent personaComponent;
//	
//	@GetMapping(value = "saluta")
//	public ResponseEntity<?> salutaRequest(@RequestParam(name = "nome", required = true) String nome)
//	{
//		return new ResponseEntity<String>("ciao " + nome, HttpStatus.OK);
//	}
//	
//	@GetMapping(value = "saluta/{nome}")
//	public ResponseEntity<?> salutaPath(@PathVariable String nome)
//	{
//		return new ResponseEntity<String>("ciao " + nome, HttpStatus.OK);
//	}
//	
//	@GetMapping(value = "calcola")
//	public ResponseEntity<?> calcolaRequest(@RequestParam(name = "numero", required = true) Integer numero, @RequestParam(name = "nome", required = true) String nome)
//	{
//		return new ResponseEntity<String>("Risultato: " + operazioniComponent.montiplicaPerLunghezzaNome(numero, nome), HttpStatus.OK);
//	}
//	
//	@GetMapping(value = "calcola/{nome}/{numero}")
//	public ResponseEntity<?> calcolaPath(@PathVariable String nome, @PathVariable Integer numero)
//	{
//		return new ResponseEntity<String>("Risultato: " + operazioniComponent.montiplicaPerLunghezzaNome(numero, nome), HttpStatus.OK);
//	}
//	
//	@GetMapping(value = "find-persona-by-codice-fiscale")
//	public ResponseEntity<?> findPersonaByCodiceFiscale(@RequestParam String codiceFiscale)
//	{
//		return new ResponseEntity<PersonaDTO>(personaComponent.findByCodiceFiscale(codiceFiscale), HttpStatus.OK);
//	}
//	
//	@PostMapping(value = "create-persona")
//	public ResponseEntity<?> createPersona(@RequestBody PersonaDTO p)
//	{
//		personaComponent.savePersona(p); 
//		return new ResponseEntity<String>(HttpStatus.CREATED);
//	}
}

