package com.prisma.dominioHr.controller;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.prisma.dominioHr.component.FileComponent;
import com.prisma.dominioHr.component.UserComponent;
import com.prisma.dominioHr.entity.File;
import com.prisma.dominioHr.service.FileService;
import com.prisma.dominioHr.utils.UploadFileResponse;

@RestController
@RequestMapping("file-controller")
public class FileController{
	
	@Autowired
	FileService fileService;
	
	@Autowired
	FileComponent fileComponent;
	
	@Autowired
	UserComponent userComponent;
	
		
//	 @PostMapping("/uploadFile")
//	    public UploadFileResponse uploadFile(@RequestParam("file") MultipartFile multiPartFile) throws IllegalStateException, IOException {
//	        File file = fileComponent.storeFile(multiPartFile);
//
//	        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
//	                .path("/download/")
//	                .path(Long.toString(file.getIdFile()))
//	                .toUriString();
//	        
//	        //Save file in local
//	        String filePath = "C:/Users/Flavio.ceccarelli/" + file.getUserAssigned().getName() + "_" + file.getUserAssigned().getSurname() + "/" + file.getLabel().getDescription() + "/"; 
//	        java.io.File dest = new java.io.File(filePath);
//	        dest.mkdirs();
//	        multiPartFile.transferTo(dest);
//
//	        return new UploadFileResponse(file.getName(), fileDownloadUri,
//	        		multiPartFile.getContentType(), multiPartFile.getSize());
//	    }
//	 
//	 @GetMapping("/download/{idFile}")
//	    public ResponseEntity<ByteArrayResource> downloadFile(@PathVariable String idFile) {
//	        // Load file from database
//	        File file = fileComponent.findByIdFile(Long.valueOf(idFile)); 
//
//	        return ResponseEntity.ok()
//	                .contentType(org.springframework.http.MediaType.parseMediaType(file.getType().getDescription()))
//	                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getName() + "\"")
//	                .body(new ByteArrayResource(file.getData()));
//	    }
	 
	
	 @GetMapping("delete-file-by-idfile")
	 public ResponseEntity<?> deleteFileByIdFile(@RequestParam String idFile){
		 fileComponent.deleteByIdFile(Long.valueOf(idFile));
		 return new ResponseEntity<String>(HttpStatus.OK);
	 }

}
