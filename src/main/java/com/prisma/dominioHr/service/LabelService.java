package com.prisma.dominioHr.service;

import com.prisma.dominioHr.entity.Label;

public interface LabelService {

	Label findByDescription(String description);

}
