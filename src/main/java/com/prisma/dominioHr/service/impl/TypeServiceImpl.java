package com.prisma.dominioHr.service.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prisma.dominioHr.entity.Type;
import com.prisma.dominioHr.repository.TypeRepository;
import com.prisma.dominioHr.service.TypeService;

@Service
@Transactional
public class TypeServiceImpl implements TypeService {

	@Autowired
	TypeRepository typeRepository;
	
	@Override
	public Type findByDescription(String description) {
		return typeRepository.findByDescription(description);
	}

}
