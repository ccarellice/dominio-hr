package com.prisma.dominioHr.service.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prisma.dominioHr.entity.Role;
import com.prisma.dominioHr.repository.RoleRepository;
import com.prisma.dominioHr.service.RoleService;


@Service
@Transactional
public class RoleServiceImpl implements RoleService {
	
	@Autowired
	RoleRepository roleRepository;
	
	@Override
	public Role findByDescription(String description) {
		return roleRepository.findByDescription(description);
	}

}
