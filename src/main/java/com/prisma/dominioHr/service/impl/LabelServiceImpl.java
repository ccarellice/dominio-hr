package com.prisma.dominioHr.service.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prisma.dominioHr.entity.Label;
import com.prisma.dominioHr.repository.LabelRepository;
import com.prisma.dominioHr.service.LabelService;

@Service
@Transactional
public class LabelServiceImpl implements LabelService {
	
	@Autowired
	LabelRepository labelRepository;
	
	@Override
	public Label findByDescription(String description) {
		return labelRepository.findByDescription(description);
	}

}
