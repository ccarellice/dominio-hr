package com.prisma.dominioHr.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prisma.dominioHr.entity.User;
import com.prisma.dominioHr.repository.UserRepository;
import com.prisma.dominioHr.service.UserService;

@Service
@Transactional
public class UserServiceImpl implements UserService {
	
	@Autowired
	UserRepository userRepository;
	
	
	@Override
	public User save(User user) {
		return userRepository.saveAndFlush(user);
	}


	@Override
	public User findByCF(String CF) {
		return userRepository.findByCF(CF);
	}


	@Override
	public void delete(User user) {
		userRepository.delete(user);
	}


	@Override
	public void deleteByCF(String CF) {
		userRepository.deleteByCF(CF);
	}


	@Override
	public User findByIdUser(Long idUser) {
		return userRepository.findByIdUser(idUser);
	}


	@Override
	public List<User> findUserByStatus(boolean status) {
		return userRepository.findUserByStatus(status);
	}


	@Override
	public List<User> findActiveStatusUserWithNoBustaPagaOnCurrentMonth(int month, int year) {
		return userRepository.findActiveStatusUserWithNoBustaPagaOnCurrentMonth(month, year);
	}


	@Override
	public int countActiveStatusUserWithNoBustaPagaOnCurrentMonth(int month, int year) {
		return userRepository.countActiveStatusUserWithNoBustaPagaOnCurrentMonth(month, year);
	}


	@Override
	public List<User> findAll() {
		return userRepository.findAll();
	}

}
