package com.prisma.dominioHr.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.prisma.dominioHr.entity.Device;
import com.prisma.dominioHr.repository.DeviceRepository;
import com.prisma.dominioHr.service.DeviceService;

@Service
@Transactional
public class DeviceServiceImpl implements DeviceService {
	
	@Autowired
	DeviceRepository deviceRepository;
	
	@Override
	public Device saveAndFlush(Device device) {
		return deviceRepository.saveAndFlush(device);
	}

	@Override
	public Device findByIdCode(String idCode) {
		return deviceRepository.findByIdCode(idCode);
	}

	@Override
	public Device findBySerialNumber(String serialNumber) {
		return deviceRepository.findBySerialNumber(serialNumber);
	}

	@Override
	public void delete(Device device) {
		deviceRepository.delete(device);
	}

	@Override
	public List<Device> findDeviceByUserCFAssigned(String CF) {
		return deviceRepository.findDeviceByUserCFAssigned(CF);
	}

	@Override
	public List<Device> findAll() {
		return deviceRepository.findAll();
	}

}
