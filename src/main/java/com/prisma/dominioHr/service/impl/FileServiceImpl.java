package com.prisma.dominioHr.service.impl;

import java.io.IOException;
import java.time.LocalDateTime;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.prisma.dominioHr.entity.File;
import com.prisma.dominioHr.repository.FileRepository;
import com.prisma.dominioHr.repository.LabelRepository;
import com.prisma.dominioHr.repository.TypeRepository;
import com.prisma.dominioHr.repository.UserRepository;
import com.prisma.dominioHr.service.FileService;

@Service
@Transactional
public class FileServiceImpl implements FileService {

	@Autowired
	FileRepository fileRepository;
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	LabelRepository labelRepository;
	
	@Autowired
	TypeRepository typeRepository;
	
	@Override
	public File save(File file) {
		return fileRepository.saveAndFlush(file);
	}

	@Override
	public File findFileByIdFile(Long idFile) {
		return fileRepository.findByIdFile(idFile);
	}

	@Override
	public void delete(File file) {
		fileRepository.delete(file);
	}

	@Override
	public void saveAndFlush(File file) {
		fileRepository.saveAndFlush(file);	
	}
	
	@Override
	public File storeFile(MultipartFile multiPartFile) {
        String originalFileName = StringUtils.cleanPath(multiPartFile.getOriginalFilename());
        
        File file = null;
		try {
			file.setLastEditDate(LocalDateTime.now());
			file.setUploadDate(LocalDateTime.now());
			file.setUserUpload(userRepository.findByCF("CCCLFV00P22H501V"));
			file.setUserAssigned(userRepository.findByCF("CNZFVN84H15D643W"));
			file.setLabel(labelRepository.findByDescription("Busta Paga"));
			file.setType(typeRepository.findByDescription(multiPartFile.getContentType()));
			file.setData(multiPartFile.getBytes());
			file.setName(originalFileName);
			
//			file = File.builder()
//					.lastEditDate(LocalDateTime.now())
//					.uploadDate(LocalDateTime.now())
//					.userUpload(userRepository.findByCF("CCCLFV00P22H501V"))
//					.userAssigned(userRepository.findByCF("CNZFVN84H15D643W"))
//					.label(labelRepository.findByDescription("Busta Paga"))
//					.type(typeRepository.findByDescription(multiPartFile.getContentType()))
//					.data(multiPartFile.getBytes())
//					.name(originalFileName)
//					.build();
			
		} catch (IOException e) {
			e.printStackTrace();
		}

        return fileRepository.save(file);
    }

	@Override
	public Page<File> findPagedFile(PageRequest pageRequest) {
		return fileRepository.findPagedFile(pageRequest);
	}

	@Override
	public int countBustaPagaCurrentMonth(int month, int year) {
		return fileRepository.countBustaPagaCurrentMonth(month, year);
	}

	@Override
	public Page<File> findLast5FileByUserId(PageRequest pageRequest, long idUser) {
		return fileRepository.findLast5FileByUserId(pageRequest, idUser);
	}

	@Override
	public long count() {
		return fileRepository.count();
	}
}


