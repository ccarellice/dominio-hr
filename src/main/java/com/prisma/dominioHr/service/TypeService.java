package com.prisma.dominioHr.service;

import com.prisma.dominioHr.entity.*;

public interface TypeService {

	Type findByDescription(String description);
}
