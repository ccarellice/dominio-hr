package com.prisma.dominioHr.service;

import java.util.List;

import com.prisma.dominioHr.entity.Device;

public interface DeviceService {

	public Device saveAndFlush(Device device);

	public Device findByIdCode(String idCode);

	public Device findBySerialNumber(String serialNumber);

	public void delete(Device device);

	public List<Device> findDeviceByUserCFAssigned(String CF);
	
	public List<Device> findAll(); 

}
