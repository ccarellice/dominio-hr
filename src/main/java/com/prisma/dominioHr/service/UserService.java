package com.prisma.dominioHr.service;

import java.util.List;

import com.prisma.dominioHr.entity.User;

public interface UserService {
	
	public abstract User save(User user);

	public abstract User findByCF(String string);

	public abstract void delete(User user);

	public abstract void deleteByCF(String CF);
	
	public abstract User findByIdUser(Long idFile);
	
	public abstract List<User> findUserByStatus(boolean status);
	
	public abstract List<User> findActiveStatusUserWithNoBustaPagaOnCurrentMonth(int month, int year);
	
	public abstract int countActiveStatusUserWithNoBustaPagaOnCurrentMonth(int month, int year);

	public abstract List<User> findAll();

}
