package com.prisma.dominioHr.service;

import com.prisma.dominioHr.entity.Role;

public interface RoleService {

	Role findByDescription(String string);

}
