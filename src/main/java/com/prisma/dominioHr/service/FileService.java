package com.prisma.dominioHr.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.web.multipart.MultipartFile;

import com.prisma.dominioHr.entity.File;

public interface FileService {

	public File save(File file);
	
	public File findFileByIdFile(Long idFile);

	public void delete(File file);
	
	public void saveAndFlush(File file);
	
	public File storeFile(MultipartFile multiPartFile);
	
	public Page<File> findPagedFile(PageRequest pageRequest);
	
	public int countBustaPagaCurrentMonth(int month, int year);
	
	public Page<File> findLast5FileByUserId(PageRequest pageRequest, long idUser);

	public long count();
	
}
