package com.prisma.dominioHr.component;

import com.prisma.dominioHr.entity.Label;

public interface LabelComponent {

	Label findByDescription(String description);

}
