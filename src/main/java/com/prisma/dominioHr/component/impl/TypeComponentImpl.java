package com.prisma.dominioHr.component.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.prisma.dominioHr.component.TypeComponent;
import com.prisma.dominioHr.entity.Type;
import com.prisma.dominioHr.service.TypeService;

@Component
public class TypeComponentImpl implements TypeComponent {
	
	@Autowired
	TypeService typeService;
	
	@Override
	public Type findByDescription(String description) {
		return typeService.findByDescription(description);
	}

}
