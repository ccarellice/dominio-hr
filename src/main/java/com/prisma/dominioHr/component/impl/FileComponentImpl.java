package com.prisma.dominioHr.component.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.prisma.dominioHr.component.FileComponent;
import com.prisma.dominioHr.converter.FileConverter;
import com.prisma.dominioHr.dto.FileDTO;
import com.prisma.dominioHr.entity.File;
import com.prisma.dominioHr.service.FileService;
import com.prisma.dominioHr.utils.DateConverter;

@Component
public class FileComponentImpl implements FileComponent {

	@Autowired
	FileService fileService;
	
	@Autowired
	FileConverter fileConverter;
	
	@Override
	public Long save(FileDTO fileDto) {
		File file = fileConverter.fromDTOToFile(fileDto);
		file = fileService.save(file);
		return file.getIdFile();
	}

	@Override
	public File findByIdFile(Long idFile) {
		return fileService.findFileByIdFile(idFile);
	}

	@Transactional
	@Override
	public void updateFile(FileDTO fileDto) {
		File file = fileService.findFileByIdFile(fileDto.getIdFile());
		file.setLastEditDate(DateConverter.fromStringToLocalDateTime(fileDto.getLastEditDate()));
	}

	@Override
	public void deleteByIdFile(Long idFile) {
		fileService.delete(fileService.findFileByIdFile(idFile));
	}
	
	@Override
	public File storeFile(MultipartFile multiPartFile) {
		return fileService.storeFile(multiPartFile);
	}

	@Override
	public Page<File> findPagedFile(PageRequest pageRequest) {
		return fileService.findPagedFile(pageRequest);
	}

	@Override
	public Page<File> findLast5FileByUserId(PageRequest pageRequest, long idUser) {
		return fileService.findLast5FileByUserId(pageRequest, idUser);
	}

	@Override
	public int countBustaPagaCurrentMonth(int month, int year) {
		return fileService.countBustaPagaCurrentMonth(month, year);
	}

	@Override
	public long count() {
		return fileService.count();
	}

}
