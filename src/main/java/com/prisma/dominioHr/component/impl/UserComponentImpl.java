package com.prisma.dominioHr.component.impl;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;

import com.prisma.dominioHr.component.UserComponent;
import com.prisma.dominioHr.converter.UserConverter;
import com.prisma.dominioHr.dto.UserDTO;
import com.prisma.dominioHr.entity.User;
import com.prisma.dominioHr.service.UserService;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.engine.util.JRSaver;
import net.sf.jasperreports.export.Exporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;

@Component
public class UserComponentImpl implements UserComponent {

	@Autowired
	UserService userService;

	@Autowired
	UserConverter userConverter;

	@Override
	public Long saveUser(UserDTO userDto) {
		User user  = userService.save(userConverter.fromDTOToUser(userDto));
		return user.getIdUser();
	}

	@Override
	public User findByCF(String CF) {
		User user = userService.findByCF(CF);
		return user;
	}

	@Override
	public void deleteUserByCF(String CF) {
		userService.deleteByCF(CF);
	}

	@Transactional
	@Override
	public void updateUser(UserDTO userDto) {
		User user = userService.findByCF(userDto.getCF());
		user.setStatus(userDto.isStatus());
	}

	@Override
	public User findByIdUser(Long idUser) {
		return userService.findByIdUser(idUser);
	}

	@Override
	public List<User> findUserByStatus(boolean status) {
		return userService.findUserByStatus(status);
	}

	@Override
	public List<User> findActiveStatusUserWithNoBustaPagaOnCurrentMonth(int month, int year) {
		return userService.findActiveStatusUserWithNoBustaPagaOnCurrentMonth(month, year);
	}

	@Override
	public int countActiveStatusUserWithNoBustaPagaOnCurrentMonth(int month, int year) {
		return userService.countActiveStatusUserWithNoBustaPagaOnCurrentMonth(month, year);
	}

	@Override
	public List<User> findAll() {
		return userService.findAll();
	}

	@Override
	public String exportUsers(String reportFormat) throws JRException, IOException {
		/*----*//*INIZIO REPORT users*//*----*/

		//INIZIO PARAMETERS REPORT users
		List<UserDTO> userDtos = userConverter.fromUsersToDTOs(userService.findAll());
		String path = "C:\\Users\\fl\\Documents\\reports";
		String reportUsersName = "/users";
		//FINE PARAMETERS REPORT users
		
		//INIZIO COMPILING REPORT users
		java.io.File file = ResourceUtils.getFile("classpath:users.jrxml");
		JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath());
		JRSaver.saveObject(jasperReport, "src/main/resources/users.jasper");
		//FINE COMPILING REPORT users

		//INIZIO POPULATING REPORT users
		JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(userDtos);
		Map<String, Object> parameters = new HashMap<>(); 
		parameters.put("createdBy", "Prisma S.r.l");
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);
		//FINE POPULATING REPORT users

		//INIZIO EXPORTING REPORT users
		if(reportFormat.equalsIgnoreCase("html")) {
			JasperExportManager.exportReportToHtmlFile(jasperPrint, path + reportUsersName + ".html");
		}
		if(reportFormat.equalsIgnoreCase("pdf")) {
			JasperExportManager.exportReportToPdfFile(jasperPrint, path + reportUsersName + ".pdf");
		}
		if(reportFormat.equalsIgnoreCase("doc")) {
			Exporter exporter = new JRDocxExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			File exportReportFile = new File(path + reportUsersName + ".docx");
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(exportReportFile));
			exporter.exportReport();
		}
		//FINE EXPORTING REPORT users

		/*----*//*FINE REPORT users*//*----*/
		return "report generated at " + path;
	}


}
