package com.prisma.dominioHr.component.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.prisma.dominioHr.component.LabelComponent;
import com.prisma.dominioHr.entity.Label;
import com.prisma.dominioHr.service.LabelService;

@Component
public class LabelComponentImpl implements LabelComponent {
	
	@Autowired
	LabelService labelService;
	
	@Override
	public Label findByDescription(String description) {
		return labelService.findByDescription(description);
	}

}
