package com.prisma.dominioHr.component.impl;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;

import com.prisma.dominioHr.component.DeviceComponent;
import com.prisma.dominioHr.converter.DeviceConverter;
import com.prisma.dominioHr.dto.DeviceDTO;
import com.prisma.dominioHr.dto.UserDTO;
import com.prisma.dominioHr.entity.Device;
import com.prisma.dominioHr.service.DeviceService;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.engine.util.JRSaver;
import net.sf.jasperreports.export.Exporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import net.sf.jasperreports.export.SimplePdfReportConfiguration;;

@Component
public class DeviceComponentImpl implements DeviceComponent {
	
	@Autowired
	DeviceService deviceService;
	
	@Autowired
	DeviceConverter deviceConverter;
	
	@Override
	public Long saveDevice(DeviceDTO deviceDto) {
		Device device = deviceService.saveAndFlush(deviceConverter.fromDTOToDeviceNoId(deviceDto));
		return device.getIdDevice();
	}

	@Override
	public DeviceDTO findByIdCode(String idCode) {
		return deviceConverter.fromDeviceToDTO(deviceService.findByIdCode(idCode));
	}

	@Override
	public Device findBySerialNumber(String serialNumber) {
		return deviceService.findBySerialNumber(serialNumber);
	}

	@Override
	public void deleteDevice(DeviceDTO deviceDto) {
		Device device = deviceConverter.fromDTOToDevice(deviceDto);
		deviceService.delete(device);
	}

	@Override
	public List<Device> findDeviceByUserCFAssigned(String CF) {
		return deviceService.findDeviceByUserCFAssigned(CF);
	}

	@Transactional
	@Override
	public void updateDevice(DeviceDTO deviceDto) {
		Device device = deviceService.findByIdCode(deviceDto.getIdCode());
		device.setSerialNumber(deviceDto.getSerialNumber());
	}

	@Override
	public List<Device> findAll() {
		return deviceService.findAll();
	}
	
	@Override
	public String exportDevices(String reportFormat) throws JRException, IOException {
		/*----*//*INIZIO REPORT devices*//*----*/

		//INIZIO PARAMETERS REPORT devices
		List<DeviceDTO> deviceDtos = deviceConverter.fromDevicesToDTOs(deviceService.findAll());
		String path = "C:\\Users\\fl\\Documents\\reports";
		String reportDevicesName = "/devices";
		//FINE PARAMETERS REPORT devices
		
		//INIZIO COMPILING REPORT devices
		java.io.File file = ResourceUtils.getFile("classpath:devices.jrxml");
		JasperReport jasperReport = JasperCompileManager.compileReport(file.getAbsolutePath());
		JRSaver.saveObject(jasperReport, "src/main/resources/devices.jasper");
		//FINE COMPILING REPORT devices

		//INIZIO POPULATING REPORT devices
		JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(deviceDtos);
		Map<String, Object> parameters = new HashMap<>(); 
		parameters.put("createdBy", "Prisma S.r.l");
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);
		//FINE POPULATING REPORT devices

		//INIZIO EXPORTING REPORT devices
		if(reportFormat.equalsIgnoreCase("html")) {
			JasperExportManager.exportReportToHtmlFile(jasperPrint, path + reportDevicesName + ".html");
		}
		if(reportFormat.equalsIgnoreCase("pdf")) {
			JasperExportManager.exportReportToPdfFile(jasperPrint, path + reportDevicesName + ".pdf");
		}
		if(reportFormat.equalsIgnoreCase("doc")) {
			Exporter exporter = new JRDocxExporter();
			exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
			File exportReportFile = new File(path + reportDevicesName + ".docx");
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(exportReportFile));
			exporter.exportReport();
		}
		//FINE EXPORTING REPORT devices

		/*----*//*FINE REPORT devices*//*----*/
		return "report generated at " + path;
	}

}
