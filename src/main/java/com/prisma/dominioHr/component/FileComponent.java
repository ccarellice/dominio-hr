package com.prisma.dominioHr.component;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.multipart.MultipartFile;

import com.prisma.dominioHr.dto.FileDTO;
import com.prisma.dominioHr.entity.File;

public interface FileComponent {

	public Long save(FileDTO fileDto);
	
	public File findByIdFile(Long idFile);

	public void updateFile(FileDTO fileDto);

	public void deleteByIdFile(Long long1);

	public File storeFile(MultipartFile multiPartFile);
	
	public Page<File> findPagedFile(PageRequest pageRequest);
	
	public Page<File> findLast5FileByUserId(PageRequest pageRequest, long idUser);
	
	public int countBustaPagaCurrentMonth(int month, int year);

	public long count();

}
