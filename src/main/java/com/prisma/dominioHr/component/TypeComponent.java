package com.prisma.dominioHr.component;

import com.prisma.dominioHr.entity.Type;

public interface TypeComponent {

	Type findByDescription(String description);

}
