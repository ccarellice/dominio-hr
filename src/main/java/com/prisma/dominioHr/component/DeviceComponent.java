package com.prisma.dominioHr.component;

import java.io.IOException;
import java.util.List;

import com.prisma.dominioHr.dto.DeviceDTO;
import com.prisma.dominioHr.entity.Device;

import net.sf.jasperreports.engine.JRException;

public interface DeviceComponent {

	public Long saveDevice(DeviceDTO deviceDto);

	public DeviceDTO findByIdCode(String idCode);

	public Device findBySerialNumber(String serialNumber);

	public void deleteDevice(DeviceDTO deviceDto);

	public 	List<Device> findDeviceByUserCFAssigned(String CF);

	public 	void updateDevice(DeviceDTO deviceDto);
	
	public 	List<Device> findAll();

	public String exportDevices(String reportFormat) throws JRException, IOException;

}
