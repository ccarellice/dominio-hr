package com.prisma.dominioHr.component;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import com.prisma.dominioHr.dto.UserDTO;
import com.prisma.dominioHr.entity.User;

import net.sf.jasperreports.engine.JRException;

public interface UserComponent {
	
	public Long saveUser(UserDTO userDto);

	public User findByCF(String CF);

	public void deleteUserByCF(String string);

	public void updateUser(UserDTO userDto);
	
	public User findByIdUser(Long idUser);
	
	public List<User> findUserByStatus(boolean status);
	
	public List<User> findActiveStatusUserWithNoBustaPagaOnCurrentMonth(int month, int year);
	
	public int countActiveStatusUserWithNoBustaPagaOnCurrentMonth(int month, int year);

	public List<User> findAll();
	
	public String exportUsers(String reportFormat) throws FileNotFoundException, JRException, IOException;

}
