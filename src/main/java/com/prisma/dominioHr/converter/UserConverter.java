package com.prisma.dominioHr.converter;

import java.util.List;

import com.prisma.dominioHr.dto.UserDTO;
import com.prisma.dominioHr.entity.User;

public interface UserConverter {
	
	public UserDTO fromUserToDTO(User user);

	public User fromDTOToUser(UserDTO userDto);
	
	public List<UserDTO> fromUsersToDTOs(List<User> users);
	
}
