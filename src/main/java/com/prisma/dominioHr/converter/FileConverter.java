package com.prisma.dominioHr.converter;

import com.prisma.dominioHr.dto.FileDTO;
import com.prisma.dominioHr.entity.File;

public interface FileConverter {

	public File fromDTOToFile(FileDTO fileDto);
	
	public FileDTO fromFileToDTO(File file);
	
}
