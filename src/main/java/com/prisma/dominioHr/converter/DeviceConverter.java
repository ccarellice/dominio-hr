package com.prisma.dominioHr.converter;

import java.util.List;

import com.prisma.dominioHr.dto.DeviceDTO;
import com.prisma.dominioHr.entity.Device;

public interface DeviceConverter {
	
	public Device fromDTOToDevice(DeviceDTO deviceDto);
	
	public DeviceDTO fromDeviceToDTO(Device device);

	public Device fromDTOToDeviceNoId(DeviceDTO deviceDto);
	
	public List<DeviceDTO> fromDevicesToDTOs(List<Device> devices);

}
