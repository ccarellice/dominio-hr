package com.prisma.dominioHr.converter.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.prisma.dominioHr.converter.FileConverter;
import com.prisma.dominioHr.dto.FileDTO;
import com.prisma.dominioHr.entity.File;
import com.prisma.dominioHr.service.LabelService;
import com.prisma.dominioHr.service.TypeService;
import com.prisma.dominioHr.service.UserService;
import com.prisma.dominioHr.utils.DateConverter;

@Component
public class FileConverterImpl implements FileConverter {
	
	@Autowired
	LabelService labelService;
	
	@Autowired
	TypeService typeService;
	
	@Autowired
	UserService userService;
	
	@Override
	public File fromDTOToFile(FileDTO fileDto) {
		File file = new File();
		file.setLastEditDate(DateConverter.fromStringToLocalDateTime(fileDto.getLastEditDate()));
		file.setUploadDate(DateConverter.fromStringToLocalDateTime(fileDto.getUploadDate()));
		file.setUserUpload(userService.findByCF(fileDto.getUserUpload()));
		file.setUserAssigned(userService.findByCF(fileDto.getUserAssigned()));
		file.setLabel(labelService.findByDescription(fileDto.getLabel()));
		file.setType(typeService.findByDescription(fileDto.getType()));
		
//		File file = File.builder()
//				.lastEditDate(DateConverter.fromStringToLocalDateTime(fileDto.getLastEditDate()))
//				.uploadDate(DateConverter.fromStringToLocalDateTime(fileDto.getUploadDate()))
//				.userUpload(userService.findByCF(fileDto.getUserUpload()))
//				.userAssigned(userService.findByCF(fileDto.getUserAssigned()))
//				.label(labelService.findByDescription(fileDto.getLabel()))
//				.type(typeService.findByDescription(fileDto.getType()))
//				.build();
		return file;
	}

	@Override
	public FileDTO fromFileToDTO(File file) {
		FileDTO fileDto = new FileDTO();
		fileDto.setLastEditDate(DateConverter.fromLocalDateTimeToString(file.getLastEditDate()));
		fileDto.setUploadDate(DateConverter.fromLocalDateTimeToString(file.getUploadDate()));
		fileDto.setUserUpload(file.getUserUpload().getCF());
		fileDto.setUserAssigned(file.getUserAssigned().getCF());
		fileDto.setLabel(file.getLabel().getDescription());
		fileDto.setType(file.getType().getDescription());
		fileDto.setIdFile(file.getIdFile());
		
//		FileDTO fileDto = FileDTO.builder()
//				.lastEditDate(DateConverter.fromLocalDateTimeToString(file.getLastEditDate()))
//				.uploadDate(DateConverter.fromLocalDateTimeToString(file.getUploadDate()))
//				.userUpload(file.getUserUpload().getCF())
//				.userAssigned(file.getUserAssigned().getCF())
//				.label(file.getLabel().getDescription())
//				.type(file.getType().getDescription())
//				.idFile(file.getIdFile())
//				.build();
		return fileDto;
	}

}
