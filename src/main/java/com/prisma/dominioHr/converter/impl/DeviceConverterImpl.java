package com.prisma.dominioHr.converter.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.prisma.dominioHr.converter.DeviceConverter;
import com.prisma.dominioHr.dto.DeviceDTO;
import com.prisma.dominioHr.entity.Device;
import com.prisma.dominioHr.service.UserService;

@Component
public class DeviceConverterImpl implements DeviceConverter {
    
	@Autowired
	private UserService userService;
	
	@Override
	public Device fromDTOToDevice(DeviceDTO deviceDto) {
		Device device = new Device();
		device.setIdDevice(Long.valueOf(deviceDto.getIdDevice()));
		device.setDescription(deviceDto.getDescription());
		device.setIdCode(deviceDto.getIdCode());
		device.setSerialNumber(deviceDto.getSerialNumber());
		device.setUserCespitiDevice(userService.findByCF(deviceDto.getUserCespitiDevice()));
		device.setUserAssignedDevice(userService.findByCF(deviceDto.getUserAssignedDevice()));
		
//		Device device = Device.builder()
//				.idDevice(Long.valueOf(deviceDto.getIdDevice()))
//				.description(deviceDto.getDescription())
//				.idCode(deviceDto.getIdCode())
//				.serialNumber(deviceDto.getSerialNumber())
//				.userCespitiDevice(userService.findByCF(deviceDto.getUserCespitiDevice()))
//				.userAssignedDevice(userService.findByCF(deviceDto.getUserAssignedDevice()))
//				.build();
		
		return device;
	}

	@Override
	public DeviceDTO fromDeviceToDTO(Device device) {
		DeviceDTO deviceDto = new DeviceDTO();
		deviceDto.setIdDevice("" + device.getIdDevice());
		deviceDto.setDescription(device.getDescription());
		deviceDto.setIdCode(device.getIdCode());
		deviceDto.setSerialNumber(device.getSerialNumber());
		deviceDto.setUserCespitiDevice(device.getUserCespitiDevice().getCF());
		deviceDto.setUserAssignedDevice(device.getUserAssignedDevice().getCF());
		
//		DeviceDTO deviceDto = DeviceDTO.builder()
//				.idDevice("" + device.getIdDevice())
//				.description(device.getDescription())
//				.idCode(device.getIdCode())
//				.serialNumber(device.getSerialNumber())
//				.userCespitiDevice(device.getUserCespitiDevice().getCF())
//				.userAssignedDevice(device.getUserAssignedDevice().getCF())
//				.build();
		
		return deviceDto;
	}

	@Override
	public Device fromDTOToDeviceNoId(DeviceDTO deviceDto) {
		Device device = new Device();
		device.setDescription(deviceDto.getDescription());
		device.setIdCode(deviceDto.getIdCode());
		device.setSerialNumber(deviceDto.getSerialNumber());
		device.setUserCespitiDevice(userService.findByCF(deviceDto.getUserCespitiDevice()));
		device.setUserAssignedDevice(userService.findByCF(deviceDto.getUserAssignedDevice()));
		
//		Device device = Device.builder()
//				.description(deviceDto.getDescription())
//				.idCode(deviceDto.getIdCode())
//				.serialNumber(deviceDto.getSerialNumber())
//				.userCespitiDevice(userService.findByCF(deviceDto.getUserCespitiDevice()))
//				.userAssignedDevice(userService.findByCF(deviceDto.getUserAssignedDevice()))
//				.build();
		
		return device;
	}

	@Override
	public List<DeviceDTO> fromDevicesToDTOs(List<Device> devices) {
		List<DeviceDTO> deviceDtos = new ArrayList<>();
		for (Device device : devices) {
			deviceDtos.add(fromDeviceToDTO(device));
		}
		return deviceDtos;
	}

}
