package com.prisma.dominioHr.converter.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.prisma.dominioHr.converter.UserConverter;
import com.prisma.dominioHr.dto.UserDTO;
import com.prisma.dominioHr.entity.User;
import com.prisma.dominioHr.service.RoleService;

@Component
public class UserConverterImpl implements UserConverter {
	
	@Autowired
	RoleService roleService;
	
	@Override
	public UserDTO fromUserToDTO(User user) {
		
		UserDTO userDto = new UserDTO();
		userDto.setIdUser("" + user.getIdUser());
		userDto.setName(user.getName());
		userDto.setSurname(user.getSurname());
		userDto.setCF(user.getCF());
		userDto.setEmail(user.getEmail());
		userDto.setPassword(user.getPassword());
		userDto.setUsername(user.getUsername());
		userDto.setStatus(user.isStatus());
		userDto.setRole(user.getRole().getDescription());
		
//		UserDTO userDto = UserDTO.builder()
//				.name(user.getName())
//				.surname(user.getSurname())
//				.CF(user.getCF())
//				.email(user.getEmail())
//				.password(user.getPassword())
//				.username(user.getUsername())
//				.status(user.isStatus())
//				.role(user.getRole().getDescription())
//				.build();
		
		return userDto;
	}
	
	@Override
	public List<UserDTO> fromUsersToDTOs(List<User> users){
		UserDTO userDto = new UserDTO();
		List<UserDTO> userDtos = new ArrayList<>();
		for (User user : users) {
			userDto = fromUserToDTO(user);
			userDtos.add(userDto);
		}
		return userDtos;
	}

	@Override
	public User fromDTOToUser(UserDTO userDto) {
		
		User user = new User();
		user.setName(userDto.getName());
		user.setSurname(userDto.getSurname());
		user.setCF(userDto.getCF());
		user.setEmail(userDto.getEmail());
		user.setPassword(userDto.getPassword());
		user.setUsername(userDto.getUsername());
		user.setStatus(userDto.isStatus());
		user.setRole(roleService.findByDescription(userDto.getRole()));
		
//		User user = User.builder()
//				.name(userDto.getName())
//				.surname(userDto.getSurname())
//				.CF(userDto.getCF())
//				.email(userDto.getEmail())
//				.password(userDto.getPassword())
//				.username(userDto.getUsername())
//				.status(userDto.isStatus())
//				.role(roleService.findByDescription(userDto.getRole()))
//				.build();
		
		return user;
	}


}
