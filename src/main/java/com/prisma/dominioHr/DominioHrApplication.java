package com.prisma.dominioHr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DominioHrApplication {

	public static void main(String[] args) {
		SpringApplication.run(DominioHrApplication.class, args);
	}

}
