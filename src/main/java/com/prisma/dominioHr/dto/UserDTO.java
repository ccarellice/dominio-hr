package com.prisma.dominioHr.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UserDTO {
	
	private String idUser;
	private String name;
	private String CF;
	private String surname;
	private String email;
	private String username;
	private String password;
	private boolean status; //false = cessato | true = attivo
	private String role;
	
//	public UserDTO(String name, String cF, String surname, String email, String username, String password,
//			boolean status, String role) {
//		super();
//		this.name = name;
//		CF = cF;
//		this.surname = surname;
//		this.email = email;
//		this.username = username;
//		this.password = password;
//		this.status = status;
//		this.role = role;
//	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCF() {
		return CF;
	}
	public void setCF(String cF) {
		CF = cF;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getIdUser() {
		return idUser;
	}
	public void setIdUser(String idUser) {
		this.idUser = idUser;
	}
	
	
	
//	public UserDTO(String name, String surname, String email, String username, String password, boolean status, String role, String CF) {
//		super();
//		this.name = name;
//		this.surname = surname;
//		this.email = email;
//		this.username = username;
//		this.password = password;
//		this.status = status;
//		this.role = role;
//		this.CF = CF;
//	}
	
	
}
