package com.prisma.dominioHr.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DeviceDTO {
	
	private String idDevice;
	private String idCode;
	private String description;
	private String userCespitiDevice;
	private String userAssignedDevice;
	private String serialNumber;
	
	
	public String getIdDevice() {
		return idDevice;
	}
	public void setIdDevice(String idDevice) {
		this.idDevice = idDevice;
	}
	public String getIdCode() {
		return idCode;
	}
	public void setIdCode(String idCode) {
		this.idCode = idCode;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getUserCespitiDevice() {
		return userCespitiDevice;
	}
	public void setUserCespitiDevice(String userCespitiDevice) {
		this.userCespitiDevice = userCespitiDevice;
	}
	public String getUserAssignedDevice() {
		return userAssignedDevice;
	}
	public void setUserAssignedDevice(String userAssignedDevice) {
		this.userAssignedDevice = userAssignedDevice;
	}
	public String getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}
	
	
	
}
