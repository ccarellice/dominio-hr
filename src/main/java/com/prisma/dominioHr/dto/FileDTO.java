package com.prisma.dominioHr.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class FileDTO {
	
	private String uploadDate;
	private String lastEditDate;
	private String userUpload;
	private String userAssigned;
	private String label;
	private String type;
	private Long idFile;
//	private String name;
	
	
	public String getUploadDate() {
		return uploadDate;
	}
	public void setUploadDate(String uploadDate) {
		this.uploadDate = uploadDate;
	}
	public String getLastEditDate() {
		return lastEditDate;
	}
	public void setLastEditDate(String lastEditDate) {
		this.lastEditDate = lastEditDate;
	}
	public String getUserUpload() {
		return userUpload;
	}
	public void setUserUpload(String userUpload) {
		this.userUpload = userUpload;
	}
	public String getUserAssigned() {
		return userAssigned;
	}
	public void setUserAssigned(String userAssigned) {
		this.userAssigned = userAssigned;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Long getIdFile() {
		return idFile;
	}
	public void setIdFile(Long idFile) {
		this.idFile = idFile;
	}
	
	
	
	
}
