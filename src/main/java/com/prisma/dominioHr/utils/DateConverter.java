package com.prisma.dominioHr.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Year;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.lang3.StringUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DateConverter {

	/** dd/MM/yyyy */
	public final static SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	/** dd/MM/yyyy HH:mm */
	public final static SimpleDateFormat sdfT = new SimpleDateFormat("dd/MM/yyyy HH:mm");
	/** dd/MM/yyyy HH:mm */
	public final static SimpleDateFormat sdfXml = new SimpleDateFormat("yyyy-MM-dd");
	/** yyyy-MM-dd */
	public final static SimpleDateFormat sdfTry = new SimpleDateFormat("yyyy/MM/dd");
	/** yyyy/MM/dd */
	public final static SimpleDateFormat sdfY = new SimpleDateFormat("yyyy");
	/** MM/yyyy */
	public final static SimpleDateFormat sdfMY = new SimpleDateFormat("MM/yyyy");
	/** yyyy/MM */
	public final static SimpleDateFormat sdfYM = new SimpleDateFormat("yyyy/MM");
	/** dd/MM/yyyy */
	public final static SimpleDateFormat sdfDMY = new SimpleDateFormat("dd/MM/yyyy");
	/** dd/MM/yyyy HH:mm:ss */
	public final static DateTimeFormatter dfDMYHMS = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
	
	
	
	private static String[] splitTrim(String regex, String date) {
		return date.trim().split(regex);
	}
	
	private static String getPattern(String date) {
		if(StringUtils.isBlank(date)) {
			return "";
		}
		String pattern = "";
		String separetor = "";
		if(date.contains("/")) {
			separetor = "/";
			
		}else if(date.contains("-")) {
			separetor = "-";
		}
		
		
		String[] array = StringUtils.isNotBlank(separetor) ? splitTrim(separetor, date) : new String[]{date};
		if(array != null) {
			for(int i=0; i<array.length; i++ ) {
				if(array[i].length() == 4) {
					pattern += "yyyy";
					if(i == 0) {
						pattern += separetor;
					}
				}else if(array[i].length() == 2) {
					if((new Integer(array[i])) > 12) {
						pattern += "dd";
						if(i != array.length-1 ) {
							pattern += separetor;
						}
					}else if((i == 0 && array.length > 2) || pattern.contains("MM")){
						if(array[i].length() == 2) {
							pattern += "dd" + (i != array.length-1 ? separetor : "");
						}else {
							pattern += "d"+ (i != array.length-1 ? separetor : "");
						} 
					}else {
						pattern += "MM";
						if(i != array.length-1 ) {
							pattern += separetor;
						}
					}
				}else {
					pattern += "d"+ (i != array.length-1 ? separetor : "");
					}
						
				}
			}
		
		return pattern;
	}
	

	public static Calendar fromLocalDateToCalendar(LocalDate localDate) {
		if(localDate == null)
			return null;
		
		Calendar calendar = Calendar.getInstance();
		calendar.set(localDate.getYear(), localDate.getMonthValue() - 1, localDate.getDayOfMonth());
		return calendar;
	}
	
	/** if dateFormat is null return format dd/MM/yyyy */
	public static String fromLocalDateToString(LocalDate localDate, SimpleDateFormat dateFormat) {
		if(localDate == null)
			return null;
		Calendar calendar = fromLocalDateToCalendar(localDate);
		return fromCalendarToString(calendar, dateFormat);
	}
	/** if dateFormat is null return format dd/MM/yyyy */
	public static String fromCalendarToString(Calendar date, SimpleDateFormat dateFormat){
		if(date == null)
			return null;
		if(dateFormat == null)
			return sdf.format(date.getTime());
		return dateFormat.format(date.getTime());
	}
	
	public static Calendar fromStringToCalendar(String date) throws ParseException{
		if(StringUtils.isBlank(date))
			return null;
		
		Calendar cal = Calendar.getInstance();
		String pattern = getPattern(date);
		if(StringUtils.isNotBlank(pattern)) {
			cal.setTime(new SimpleDateFormat(pattern).parse(date));
			return cal;
		}
		
		return null;
		
	}
	
	public static String fromDateToString(Date date, SimpleDateFormat dateFormat) {
		if(date == null)
			return null;
		if(dateFormat == null)
			return sdf.format(date);
		return dateFormat.format(date);
	}
	
	public static Date fromStringToDate(String date) throws ParseException {
		
		if(StringUtils.isBlank(date))
			return null;
		String pattern = getPattern(date);
		if(StringUtils.isBlank(pattern)) {
			return null;
		}
		return new SimpleDateFormat(pattern).parse(date);
	}
	
	public static Calendar fromDateToCalendar(Date data) throws ParseException{
		
		Calendar cal = null;
		if(data != null){
			cal = Calendar.getInstance();
			cal.setTime(data);
		}
		return cal;
	}
	
	public static Calendar fromXMLGregorianCalendarToCalendar(XMLGregorianCalendar xml) throws ParseException {
		if(xml == null)
			return null;
		return fromDateToCalendar(xml.toGregorianCalendar().getTime());	
		
	}
	
	public static XMLGregorianCalendar toXMLGregorianCalendar(String data) throws DatatypeConfigurationException, ParseException {
		if(StringUtils.isBlank(data))
			return null;
		String pattern = getPattern(data);
		if(StringUtils.isNotBlank(pattern) && pattern.equals(sdf.toPattern())) {
			return DatatypeFactory.newInstance().newXMLGregorianCalendar(sdfXml.format(sdf.parse(data)));
		}
		
		return null;
	}
	
	public static LocalDate fromStringToLocalDate(String date) {
		if(StringUtils.isBlank(date))
			return null;
		String pattern = getPattern(date);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(StringUtils.isNotBlank(pattern) ? pattern : "yyyy/MM/d");
		try {
			return LocalDate.parse(date, formatter);
		} catch (Exception e) {
			return null;
		}
	}
	
	public static int compareLocalDateDESC(LocalDate d1, LocalDate d2) {
		if(d1 == null && d2 == null) {
			return 0;
		}
		if(d2 == null) {
			return 1;
		}
		if(d1.isAfter(d2)) {
			return 1;
		}else if(d1.isEqual(d2)) {
			return 0;
		}
		return -1;
	}
	public static int compareLocalDateASC(LocalDate d1, LocalDate d2) {
		if(d1 == null && d2 == null) {
			return 0;
		}
		if(d2 == null) {
			return -1;
		}
		if(d1.isAfter(d2)) {
			return -1;
		}else if(d1.isEqual(d2)) {
			return 0;
		}
		return 1;
	}
	
//	public static boolean stringDateIsNotExpired(String value) {
//		Calendar dateValue = null;
//		try {
//			dateValue = fromStringToCalendar(value);
//		} catch (ParseException e) {
//			log.error(value + "Errore" );
//		}catch (Exception e) {
//			log.error(value + "Errore" );
//		}
//		if(dateValue != null && (dateValue.compareTo(Calendar.getInstance()) >= 0 )) {
//			return true;
//		}
//		
//		return false;
//	}
	
	public static String stringDateInDMY(String date) {
		if(StringUtils.isBlank(date) || date.contains("n.a")) {
			return "n.a";
		}
		String pattern = getPattern(date);
		if(StringUtils.isBlank(pattern)) {
			return "n.a";
		}
		if(sdfY.toPattern().equals(pattern)) {
			return date;
		}
		if(pattern.length() == 7) {
			YearMonth yearMonth = fromStringToYearMonth(date, pattern);
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern(sdfMY.toPattern());
			if(formatter != null) {
				return yearMonth.format(formatter);
			}
			
		}
		LocalDate localDate = fromStringToLocalDate(date);
		if(localDate != null) {
			if(sdf.toPattern().equals(pattern)) {
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern(sdfDMY.toPattern());
				if(formatter != null) {
					return localDate.format(formatter);
				}
			}
			
		}
		
		return "";
	}
	
	public static YearMonth fromStringToYearMonth(String date, String pattern) {
		if(StringUtils.isBlank(date))
			return null;
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(StringUtils.isNotBlank(pattern) ? pattern : "yyyy/MM/d");
		if(StringUtils.isBlank(date))
			return null;
		try {
			return YearMonth.parse(date, formatter);
		} catch (Exception e) {
			return null;
		}
		
	}
	
	public static Year fromStringToYear(String date, String pattern) {
		if(StringUtils.isBlank(date))
			return null;
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(StringUtils.isNotBlank(pattern) ? pattern : "yyyy/MM/d");
		if(StringUtils.isBlank(date))
			return null;
		try {
			return Year.parse(date, formatter);
		} catch (Exception e) {
			return null;
		}
		
	}
	
	public static boolean isExipred(String dataString) {
		if(StringUtils.isBlank(dataString)) {
			return false;
		}
		LocalDate now = LocalDate.now();
		String pattern = getPattern(dataString);
		if(StringUtils.isBlank(pattern)) {
			return false;
		}
		if(sdfY.toPattern().equals(pattern)) {
			return Year.from(now).isAfter(fromStringToYear(dataString, pattern));
		}
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
		if(pattern.length() == 7) {
			YearMonth yearMonth = fromStringToYearMonth(dataString, pattern);
			if(formatter != null) {
				return YearMonth.from(now).isAfter(yearMonth);
			}
			
		}
		return now.isAfter(LocalDate.parse(dataString, formatter));
		
	}
	
	public static String fromLocalDateTimeToString(LocalDateTime localDateTime) {
		localDateTime = LocalDateTime.now();
		return localDateTime.format(dfDMYHMS);
	}
	
	public static LocalDateTime fromStringToLocalDateTime(String stringDateTime) {
		return LocalDateTime.parse(stringDateTime, dfDMYHMS);
	}
	
//	public static String getCurrentMonth() {
//		return "" + LocalDate.now().getMonthValue();
//	}
}
