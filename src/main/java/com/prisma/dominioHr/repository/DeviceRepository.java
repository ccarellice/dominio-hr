package com.prisma.dominioHr.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.prisma.dominioHr.entity.Device;

@Repository
public interface DeviceRepository extends JpaRepository<Device, Long> {

	public Device findByIdCode(String idCode);

	public Device findBySerialNumber(String serialNumber);
	
	@Query("SELECT d FROM Device d JOIN d.userAssignedDevice u WHERE u.CF = ?1")
	public List<Device> findDeviceByUserCFAssigned(String CF);
	
	@Query("SELECT d FROM Device d JOIN d.userAssignedDevice u WHERE u.idUser = ?1")
	public List<Device> findDeviceByUserIdUser(long idUser);
	
}
