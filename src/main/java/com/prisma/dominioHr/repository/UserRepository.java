package com.prisma.dominioHr.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.prisma.dominioHr.entity.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long>{
	
	public abstract User findByCF(String CF);
	
	public abstract void delete(User user);

	public abstract void deleteByCF(String cF);
	
	public abstract User findByIdUser(Long idUser);
	
	public abstract List<User> findUserByStatus(boolean status);
	
	@Query("SELECT u FROM User u JOIN u.fileAssigned f WHERE f.label = 12 AND month(f.uploadDate) <> ?1 AND year(f.uploadDate) = ?2 AND u.status = true" +
	" OR f.label <> 12 AND month(f.uploadDate) <> ?1 AND year(f.uploadDate) = ?2 AND u.status = true GROUP BY u.CF")
	public abstract List<User> findActiveStatusUserWithNoBustaPagaOnCurrentMonth(int month, int year);
	
//	
	@Query("SELECT COUNT(u) FROM User u JOIN u.fileAssigned f WHERE f.label = 12 AND month(f.uploadDate) <> ?1 AND year(f.uploadDate) = ?2 GROUP BY u.CF")
	public abstract Integer countActiveStatusUserWithNoBustaPagaOnCurrentMonth(int month, int year);
}
