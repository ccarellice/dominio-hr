package com.prisma.dominioHr.repository;



import java.time.LocalDate;
import java.time.Month;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.prisma.dominioHr.entity.File;
import com.prisma.dominioHr.entity.Label;
import com.prisma.dominioHr.utils.DateConverter;

@Repository
public interface FileRepository extends JpaRepository<File, Long>{
	
	public abstract File findByIdFile(Long idFile);
	
	public abstract void delete(File file);
	
	@Query(value = "SELECT f FROM File f ORDER BY f.uploadDate DESC")
	public abstract Page<File> findPagedFile(Pageable pageRequest);
	
	@Query("SELECT COUNT(f) FROM File f WHERE month(f.uploadDate) = ?1 AND f.label = 12 AND year(f.uploadDate) = ?2")
	public abstract int countBustaPagaCurrentMonth(int month, int year);
	
	@Query(value = "SELECT f FROM File f WHERE f.userAssigned.idUser = :idUser ORDER BY f.uploadDate DESC")
	public abstract Page<File> findLast5FileByUserId(Pageable pageRequest, @Param("idUser")long idUser);
}
