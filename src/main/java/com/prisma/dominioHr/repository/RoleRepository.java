package com.prisma.dominioHr.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.prisma.dominioHr.entity.Role;
import com.prisma.dominioHr.entity.User;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long>{

	public abstract Role findByDescription(String description);

}
