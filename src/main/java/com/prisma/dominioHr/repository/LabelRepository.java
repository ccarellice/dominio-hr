package com.prisma.dominioHr.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.prisma.dominioHr.entity.*;

@Repository
public interface LabelRepository extends JpaRepository<Label, Long>{

	public abstract Label findByDescription(String description);
}
