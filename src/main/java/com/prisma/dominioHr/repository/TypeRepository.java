package com.prisma.dominioHr.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.prisma.dominioHr.entity.Type;

import aj.org.objectweb.asm.Label;

@Repository
public interface TypeRepository extends JpaRepository<Type, Long>{

	public abstract Type findByDescription(String description);

}
